#!/bin/bash

cd "$WORKDIR"/public_html &&
 composer install --working-dir="$WORKDIR"/public_html &&
 php artisan migrate &&
 chmod 777 -R storage &&
 nohup php artisan queue:work --daemon > storage/logs/queue.log &
 php-fpm