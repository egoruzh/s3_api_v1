
# Documents storage API


> #### File encryption :
>
> `AES-256-CBC`

> #### Endpoints :
>
>
> `/api/v1/user/{user_id}/upload`
>
> `/api/v1/user/{user_id}/download`
>
> `/api/v1/user/{user_id}/files`
>
> `/api/v1/user/{user_id}/folders`

## Deployment

To deploy this project run
```bash
  docker-compose up -d
```


## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`AWS_ACCESS_KEY_ID`

`AWS_SECRET_ACCESS_KEY`

`AWS_DEFAULT_REGION=us-east-2`

`AWS_BUCKET`

`FILE_VAULT_KEY`

## Usage

**BASE_URL**  : `127.0.0.1/api/v1/user`

### Upload file user root folder
```
POST [BASE_URL]/{user_id}/upload
```

>
> #### POSTFIELDS
>
>
>  `userFile[]` (multipart/form-data)
>
>  `doctype`   (`idcard`,`pasport`,`other`)

```
{
  "status": "success",
  "location": "{user_id}",
  "data": [
    {
      "message": "File successfully uploaded",
      "file": "in_root(9).txt",
      "uploaded": true
    },
    {
      "message": "The file must be a file of type: image\/jpeg, image\/png, application\/pdf, text\/plain.",
      "file": "",
      "uploaded": false
    },
    {
      "message": "File successfully uploaded",
      "file": "Invoice # 1(1).pdf",
      "uploaded": true
    }
  ]
}
```
### Upload file to custom folder
```
POST [BASE_URL]/{user_id}/upload/?path=folder1/folder2&doctype=docs
```
#### POST Params

>
>  `userFile[]` (multipart/form-data)
>
>  `doctype`   (`idcard`,`pasport`,`other`)

```
{
  "status": "success",
  "location": "{user_id}/folder1/folder2",
  "data": [
    {
      "message": "File successfully uploaded",
      "file": "filename.txt",
      "uploaded": true
    },
    {
      "message": "The file must be a file of type: image\/jpeg, image\/png, application\/pdf, text\/plain.",
      "file": false,
      "uploaded": false
    }
  ]
}
```
### Get file content

> param: `path` `filename`

```
GET [BASE_URL]/{user_id}/download/?path=folder1/folder2/folder3/folder4&filename=file_in_folder_1_2_3_4.txt
```

### Download file
```
GET [BASE_URL]/{user_id}/download/?download=1&path=folder1/folder2/folder3/folder4&filename=file_in_folder_1_2_3_4.txt
```
> param: `download`

### Download user directory zip archive

```
GET [BASE_URL]/{user_id}/download/?path=folder1/folder2/folder3/folder4
```

```
GET [BASE_URL]/{user_id}/download/?path=folder1/folder2/folder3
```

```
GET [BASE_URL]/{user_id}/download/?path=folder1/folder2
```

### List user files and folders in root directory

```
GET [BASE_URL]/{user_id}/files
```
```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011",
    "files": [
      {
        "type": "dir",
        "name": "folder1"
      },
      {
        "type": "dir",
        "name": "folder1_2"
      },
      {
        "type": "file",
        "name": "in_root.txt",
        "path": ".",
        "size": 973,
        "UploadedTime": "5 seconds ago",
        "created_at": "2021-11-19T07:19:23.000000Z"
      }
    ]
  }
}
```

### List user files and folders in specified directory
```
GET [BASE_URL]/{user_id}/files?path=folder1
```

> param: `path`

```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011\/folder1",
    "files": [
      {
        "type": "dir",
        "name": "folder2"
      }
    ]
  }
}
```

```
GET [BASE_URL]/{user_id}/files?path=folder1/folder2
```
```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011\/folder1\/folder2",
    "files": [
      {
        "type": "dir",
        "name": "folder3"
      },
      {
        "type": "file",
        "name": "file_in_folder_1_2.txt",
        "path": ".",
        "size": 4,
        "UploadedTime": "18 hours ago",
        "created_at": "2021-11-18T13:34:11.000000Z"
      }
    ]
  }
}
```
### List user files and folders in specified directory recursive

> param: `path`
>
> param: `recursive`
```
GET [BASE_URL]/{user_id}/files?path=folder1&recursive=1
```
```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011\/folder1",
    "files": [
      {
        "type": "dir",
        "name": "folder2"
      },
      {
        "type": "dir",
        "name": "folder2\/folder3"
      },
      {
        "type": "dir",
        "name": "folder2\/folder3\/folder4"
      },
      {
        "type": "file",
        "name": "file_in_folder_1_2.txt",
        "path": "folder2",
        "size": 4,
        "UploadedTime": "18 hours ago",
        "created_at": "2021-11-18T13:34:11.000000Z"
      },
      {
        "type": "file",
        "name": "file_in_folder_1_2_3_4.txt",
        "path": "folder2\/folder3\/folder4",
        "size": 27,
        "UploadedTime": "18 hours ago",
        "created_at": "2021-11-18T13:31:50.000000Z"
      }
    ]
  }
}
```
```
GET [BASE_URL]/{user_id}/files?path=folder1/folder2&recursive=1
```
```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011\/folder1\/folder2",
    "files": [
      {
        "type": "dir",
        "name": "folder3"
      },
      {
        "type": "dir",
        "name": "folder3\/folder4"
      },
      {
        "type": "file",
        "name": "file_in_folder_1_2.txt",
        "path": ".",
        "size": 4,
        "UploadedTime": "18 hours ago",
        "created_at": "2021-11-18T13:34:11.000000Z"
      },
      {
        "type": "file",
        "name": "file_in_folder_1_2_3_4.txt",
        "path": "folder3\/folder4",
        "size": 27,
        "UploadedTime": "18 hours ago",
        "created_at": "2021-11-18T13:31:50.000000Z"
      }
    ]
  }
}
```
### List user folders
```
GET [BASE_URL]/{user_id}/folders?path=folder1
```

```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011\/folder1",
    "folders": [
      "folder2"
    ]
  }
}
```
```
GET [BASE_URL]/{user_id}/folders?path=folder1&recursive=1
```

```
{
  "data": {
    "location": "613bd7f0-80ae-4fdd-b345-1a109f004011\/folder1",
    "folders": [
      "folder2",
      "folder2\/folder3",
      "folder2\/folder3\/folder4"
    ]
  }
}
```
