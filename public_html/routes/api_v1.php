<?php

use App\Http\Controllers\Api\V1\FileController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/user/{id}'], function () {
    Route::get('files', [FileController::class, 'index'])->name('files');
    Route::post('upload', [FileController::class, 'store'])->name('upload');
    Route::get('download', [FileController::class, 'downloadFile'])->name('downloadfile');
    Route::get('folders', [FileController::class, 'getUserDirNamesByDir'])->name('folders');
    Route::get('unlinkuserdir', [FileController::class, 'unlinkUserDir'])->name('unlinkuserdir');
    Route::get('unlink', [FileController::class, 'unlinkUserFile'])->name('unlinkUserFile');
});

Route::fallback(function () {
    return response()->json(['error' => 'Route Not Found!'], 404);
});
