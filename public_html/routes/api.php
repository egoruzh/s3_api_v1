<?php

use Illuminate\Support\Facades\Route;

Route::get('/ping', static function () {
    return ["status" => 200, "timestamp" => time()];
});

Route::fallback(function () {
    return response()->json(['error' => 'Route Not Found!'], 404);
});
