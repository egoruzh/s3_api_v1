<?php

namespace Converter;

use Exception;
use FormulaParser\FormulaParser;

class Calculator
{

    public const THROUGH_DEPTH = 5;

    /**
     * @param string $str 'dz+3'
     * @param array $vars ['dx'=> 1, 'dy'=> 1.3, 'dz' => 2.3]
     * @return float
     */
    public static function getSumByString(string $str, array $vars): float
    {
        if ($str === 'throughDepth' || $str === 'throughBoreDepth') {
            $str = 'dz+' . self::THROUGH_DEPTH;
        }
        if ($str === "0") {
            return 0.0;
        }
        $variables = [];
        $replace = [
            'dx' => 'x',
            'dy' => 'y',
            'dz' => 'z',
        ];
        $formula = str_replace(['dx', 'dy', 'dz'], ['x', 'y', 'z'], $str);

        if ((float)$formula > 0) {
            return round((float)$formula, 1);
        }

        try {
            $parser = new FormulaParser($formula, 1);
            foreach ($vars as $key => $var) {
                $variables[$replace[$key]] = $var;
            }
            $parser->setVariables($variables);
            $result = $parser->getResult()[1];
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return (float)$result;
    }
}