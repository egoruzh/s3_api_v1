<?php

namespace Converter;

use DateTimeZone;
use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log extends Logger
{
    public function __construct() {
        parent::__construct('default');
        $this->pushHandler(new StreamHandler(__DIR__.'/../files/app.log',Level::Info));
    }

    public function add(Level $level = Level::Info, string $title = '', $data = false)
    {
        $title = $title === '' ? 'undefined' : $title;
        $ip = match (true) {
            !empty($_SERVER['HTTP_CLIENT_IP']) => $_SERVER['HTTP_CLIENT_IP'],
            !empty($_SERVER['HTTP_X_FORWARDED_FOR']) => $_SERVER['HTTP_X_FORWARDED_FOR'],
            default => $_SERVER['REMOTE_ADDR'],
        };
        $logData['IP'] = $ip;
        $logData['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
        $logData['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'] ?? '';
        $logData['REQUEST_METHOD'] = $_SERVER['REQUEST_METHOD'];
        if($level === Level::Error){
            $logData['REQUEST'] = $_REQUEST;
            $logData['REQUEST_DATA'] = file_get_contents('php://input');
            $logData['result'] =   $data;
        }

        $this->log($level, $title, $logData);
    }
}