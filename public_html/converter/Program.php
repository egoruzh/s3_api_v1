<?php

namespace json2xml;

use SimpleXMLElement;

class Program
{
    public SimpleXMLElement $xml;
    public array $detail;
    public array $program;
    public const TOOL_MILL = [
        'name' => 'Mill20',
        'comment' => 'Фреза короткая d=20мм L=28мм',
        'd' => '20',
    ];
    public const BORE_THROUGH_DZ_PLUS = 3;
    public const TOOL_CUT = [
        'name' => 'Cut4',
        'comment' => 'Пазовальная пила толщиной 4мм',
        'd' => '4',
    ];

    public const TOOL_BUTT_GR = [
        'name' => 'Dmill3',
        'comment' => 'Пазовальная пила толщиной 3мм для торцевых пазов',
        'd' => '20',
    ];


    public bool $side;
    public const BORE_SIDE = [
        'front' => 'bf',
        'back' => 'bf',
        'left' => 'bl',
        'right' => 'br',
        'bottom' => 'bt',
        'top' => 'bb',
    ];
    public const BORE_SIDE_NAMES = [
        'bf' => 'Bore',
        'bl' => 'BoreLeft_d',
        'br' => 'BoreRight_d', // BoreRight_d8
        'bt' => 'BoreUp_d',
        'bb' => 'BoreDown_d',
    ];

    public function __construct(array $program, array $detail, bool $side = true)
    {
        $this->xml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?><program></program>');
        $this->detail = $detail;
        $this->program = $program;
        $this->side = $side;

        self::setAttributes();
        self::setTool();
        self::setBore();
        self::setCorners();
        self::setRects();
    }

    private function setAttributes(): void
    {
        $this->xml->addAttribute('dx', $this->detail['l']);
        $this->xml->addAttribute('dy', $this->detail['h']);
        $this->xml->addAttribute('dz', $this->detail['dz']);
    }

    private function setBore(): void
    {
        foreach ($this->program['holes'] ?? [] as $bore) {
            $x = $bore['x_axis'] === 'right' ? $this->detail['l'] - $bore['x'] : $bore['x'];
            $y = $bore['y_axis'] === 'top' ? $this->detail['h'] - $bore['y'] : $bore['y'];
            //$this->detail['h'] - $corner['y']
            $oXml = $this->xml->addChild(self::BORE_SIDE[$bore['side']]);
            if (self::BORE_SIDE[$bore['side']] != 'bf') {
                $oXml->addAttribute('m', 'false');
                $oXml->addAttribute('z', $bore['z']);
                $oXml->addAttribute('ver', '2');
            } elseif ($bore['depth'] >= $this->detail['dz']) {
                $bore['depth'] = $this->detail['dz'] + self::BORE_THROUGH_DZ_PLUS;
            }
            $oXml->addAttribute('x', $x);
            $oXml->addAttribute('y', $y);
            $oXml->addAttribute('dp', (string)$bore['depth']);
            $oXml->addAttribute('ac', 1);
            $oXml->addAttribute('av', 'false');
            //$oXml->addAttribute('name', self::BORE_SIDE_NAMES[self::BORE_SIDE[$bore['side']]] . $bore['diam']);
            $oXml->addAttribute('name', 'Bore' . $bore['diam']);
            $oXml->addAttribute('comment', $bore['comment'] ?? '');
        }
    }

    private function setTool(): void
    {
        foreach ($this->program['rects'] ?? [] as $rect) {
            $min = min($rect['width'], $rect['height']);
//            for ($i = 4; $i<=20; $i+2) {
//                echo PHP_EOL . $i;
//            }
//            if($min / 20 > 1){
//
//            }
//            s($min, $min % 20, $min / 20);
        }
        $toolsArray = [];
        if (
            (isset($this->program['rects']) && !empty($this->program['rects'])) ||
            (isset($this->program['corners']) && !empty($this->program['corners']))
        ) {
            $tool = $this->xml->addChild('tool');
            $tool->addAttribute('comment', self::TOOL_MILL['comment']);
            $tool->addAttribute('name', self::TOOL_MILL['name']);
            $tool->addAttribute('d', self::TOOL_MILL['d']);

            $tool = $this->xml->addChild('tool');
            $tool->addAttribute('comment', self::TOOL_CUT['comment']);
            $tool->addAttribute('name', self::TOOL_CUT['name']);
            $tool->addAttribute('d', self::TOOL_CUT['d']);

            $tool = $this->xml->addChild('tool');
            $tool->addAttribute('comment', self::TOOL_BUTT_GR['comment']);
            $tool->addAttribute('name', self::TOOL_BUTT_GR['name']);
            $tool->addAttribute('d', self::TOOL_BUTT_GR['d']);

        }
        if (isset($this->program['holes'])) {
            foreach ($this->program['holes'] as $bore) {
                $toolsArray[(string)$bore['diam']] = $bore;
            }
            foreach ($toolsArray as $value) {
                $tool = $this->xml->addChild('tool');
//                $name = self::BORE_SIDE_NAMES[self::BORE_SIDE[$value['side']]] ?? 'Bore';
                $tool->addAttribute('name', 'Bore' . $value['diam']);
                $tool->addAttribute('d', $value['diam']);
            }
        }
    }

    private function setCorners(): void
    {
        if (isset($this->program['corners'])) {
            foreach ($this->program['corners'] as $corner) {
                $ms = $this->xml->addChild('ms');
                $ms->addAttribute('dp', $this->detail['material']['thickness'] + 3);
                $ms->addAttribute('in', '202');
                $ms->addAttribute('out', '202');
                $ms->addAttribute('c', '2');
                $ms->addAttribute('name', 'Mill20');
                $ms->addAttribute('comment', $corner['comment'] ?? '');
                $ml = $this->xml->addChild('ml');
                if ($corner['type'] == 'line') {
                    switch (true) {
                        case $corner['angle'] == 'right_top':
                            $ms->addAttribute('x', $this->detail['l']);
                            $ms->addAttribute('y', $this->detail['h'] - $corner['y']);
                            $ml->addAttribute('x', $this->detail['l'] - $corner['x']);
                            $ml->addAttribute('y', $this->detail['h']);
                            break;
                        case $corner['angle'] == 'left_bottom':
                            $ms->addAttribute('x', '0');
                            $ms->addAttribute('y', $corner['y']);
                            $ml->addAttribute('x', $corner['x']);
                            $ml->addAttribute('y', '0');
                            break;
                        case $corner['angle'] == 'left_top':
                            $ms->addAttribute('x', $corner['x']);
                            $ms->addAttribute('y', $this->detail['h']);
                            $ml->addAttribute('x', '0');
                            $ml->addAttribute('y', $this->detail['h'] - $corner['y']);
                            break;
                        case $corner['angle'] == 'right_bottom':
                            $ms->addAttribute('x', $this->detail['l'] - $corner['x']);
                            $ms->addAttribute('y', '0');
                            $ml->addAttribute('x', $this->detail['l']);
                            $ml->addAttribute('y', $corner['y']);
                            break;
                    }
                } else {
                    switch (true) {
                        case $corner['angle'] == 'right_top':
                            $ms->addAttribute('x', $this->detail['l']);
                            $ms->addAttribute('y', '-5');
                            $ml->addAttribute('x', $this->detail['l']);
                            $ml->addAttribute('y', $this->detail['h'] - $corner['r']);
                            $mac = $this->xml->addChild('mac');
                            $mac->addAttribute('x', $this->detail['l'] - $corner['r']);
                            $mac->addAttribute('y', $this->detail['h']);
                            $mac->addAttribute('cx', $this->detail['l'] - $corner['r']);
                            $mac->addAttribute('cy', $this->detail['h'] - $corner['r']);
                            $mac->addAttribute('dir', 'true');
                            $ml = $this->xml->addChild('ml');
                            $ml->addAttribute('x', '-5');
                            $ml->addAttribute('y', $this->detail['h']);
                            break;
                        case $corner['angle'] == 'left_bottom':
                            $ms->addAttribute('x', '0');
                            $ms->addAttribute('y', $this->detail['h'] + 5);
                            $ml->addAttribute('x', '0');
                            $ml->addAttribute('y', $corner['r']);
                            $mac = $this->xml->addChild('mac');
                            $mac->addAttribute('x', $corner['r']);
                            $mac->addAttribute('y', '0');
                            $mac->addAttribute('cx', $corner['r']);
                            $mac->addAttribute('cy', $corner['r']);
                            $mac->addAttribute('dir', 'true');
                            $ml = $this->xml->addChild('ml');
                            $ml->addAttribute('x', $this->detail['l'] + 5);
                            $ml->addAttribute('y', '0');
                            break;
                        case $corner['angle'] == 'left_top':
                            $ms->addAttribute('x', $this->detail['l'] + 5);
                            $ms->addAttribute('y', $this->detail['h']);
                            $ml->addAttribute('x', $corner['r']);
                            $ml->addAttribute('y', $this->detail['h']);
                            $mac = $this->xml->addChild('mac');
                            $mac->addAttribute('x', '0');
                            $mac->addAttribute('y', $this->detail['h'] - $corner['r']);
                            $mac->addAttribute('cx', $corner['r']);
                            $mac->addAttribute('cy', $this->detail['h'] - $corner['r']);
                            $mac->addAttribute('dir', 'true');
                            $ml = $this->xml->addChild('ml');
                            $ml->addAttribute('x', '0');
                            $ml->addAttribute('y', '-5');
                            break;
                        case $corner['angle'] == 'right_bottom':
                            $ms->addAttribute('x', '-5');
                            $ms->addAttribute('y', '0');
                            $ml->addAttribute('x', $this->detail['l'] - $corner['r']);
                            $ml->addAttribute('y', '0');
                            $mac = $this->xml->addChild('mac');
                            $mac->addAttribute('x', $this->detail['l']);
                            $mac->addAttribute('y', $corner['r']);
                            $mac->addAttribute('cx', $this->detail['l'] - $corner['r']);
                            $mac->addAttribute('cy', $corner['r']);
                            $mac->addAttribute('dir', 'true');
                            $ml = $this->xml->addChild('ml');
                            $ml->addAttribute('x', $this->detail['l']);
                            $ml->addAttribute('y', $this->detail['h'] + 5);
                            break;
                    }
                }
            }
        }
    }

    private function setRects(): void
    {
        if (isset($this->program['rects'])) {
            foreach ($this->program['rects'] as $rect) {
                $fullDepth = $rect['depth'] >= $this->detail['material']['thickness'];
                $extXY = $rect['width'] >= $this->detail['l'] || $rect['height'] >= $this->detail['h'];
                $rect['r'] = $extXY ? 0 : $rect['r'];
                $rect['z'] = $rect['z'] ?? 0;
//                $extWidth = $this->detail['l'] == $rect['width'] || $this->detail['h'] == $rect['width'];
//                $extHeight = $this->detail['l'] == $rect['height'] || $this->detail['h'] == $rect['height'];
//                $ext = $extHeight || $extWidth;

                if ($fullDepth !== true) {
                    if ((float)$rect['r'] <= 10 && $rect['z'] == 0) {
//                if ($fullDepth !== true) {
//                    if ((float)$rect['r'] == 0 && $rect['z'] == 0 && !$ext) {
//                    } elseif ((float)$rect['r'] == 0 && $rect['z'] == 0) {
                        $t = min($rect['width'], $rect['height']);
                        $gr = $this->xml->addChild('gr');
                        $gr->addAttribute('x1', $rect['x']);
                        $gr->addAttribute('y1', $rect['y']);
                        $gr->addAttribute(
                            'x2',
                            $rect['width'] <= $rect['height'] ? $rect['x'] : max(
                                    $rect['width'],
                                    $rect['height']
                                ) + $rect['x']
                        );
                        $gr->addAttribute(
                            'y2',
                            $rect['width'] <= $rect['height'] ? $rect['y'] + $rect['height'] : $rect['y']
                        );
                        $gr->addAttribute('t', $t);
                        $gr->addAttribute('c', $rect['width'] <= $rect['height'] ? '2' : '1');
                        $gr->addAttribute('dp', $rect['depth']);
                        $gr->addAttribute('name', self::TOOL_CUT['name']);
                        $gr->addAttribute('comment', $rect['comment'] ?? '');

                    } elseif ($rect['z'] > 0) {
                        $rectT = min($rect['width'], $rect['height']);
                        $rectH = max($rect['width'], $rect['height']);
                        $operationCount = ceil($rectT / 2.5) == 1 ? 1 : ceil($rectT / 2);
                        for ($i = 1; $i <= $operationCount; $i++) {
                            if ($i == $operationCount) {
                                $dp = $this->detail['thickness'] - ($rect['z'] + $rectT) + 2.5;
                            } else {
                                $dp = $this->detail['thickness'] - ((($i - 1) * 2) + $rect['z']);
                            }
                            switch (true) {
                                case $rect['side'] == 'left':
                                    if ($rectH == $this->detail['h']) {
                                        $msY = (-$i * 5) - 50;
                                        $mlY = $this->detail['h'] + 70;
                                    } else {
                                        $msY = $rect['y'];
                                        $mlY = $rect['y'] + $rectH;
                                    }
                                    $msX = $rect['depth'];
                                    $mlX = $rect['depth'];
                                    break;
                                case $rect['side'] == 'right':
                                    if ($rectH == $this->detail['h']) {
                                        $msY = $rectH + ($i * 5) + 50;
                                        $mlY = -70;
                                    } else {
                                        $msY = $rect['y'] + $rectH;
                                        $mlY = $rect['y'];
                                    }
                                    $msX = $this->detail['l'] - $rect['depth'];
                                    $mlX = $this->detail['l'] - $rect['depth'];
                                    break;
                                case $rect['side'] == 'top':
                                    if ($rectH == $this->detail['l']) {
                                        $msX = -(50 + ($i * 5));
                                        $mlX = $rectH + 70;
                                    } else {
                                        $msX = $rect['x'];
                                        $mlX = $rect['x'] + $rectH;
                                    }
                                    $msY = $this->detail['h'] - $rect['depth'];
                                    $mlY = $this->detail['h'] - $rect['depth'];
                                    break;
                                case $rect['side'] == 'bottom':
                                    if ($rectH == $this->detail['l']) {
                                        $msX = $rectH + ($i * 5) + 50;
                                        $mlX = -70;
                                    } else {
                                        $msX = $rect['x'] + $rectH;
                                        $mlX = $rect['x'];
                                    }
                                    $msY = $rect['depth'];
                                    $mlY = $rect['depth'];
                                    break;
                            }

                            $ms = $this->xml->addChild('ms');
                            $ms->addAttribute('dp', $dp);//$this->detail['thickness'] + 3
                            $ms->addAttribute('comment', $rect['comment'] ?? '');
                            $ms->addAttribute('x', $msX);
                            $ms->addAttribute('y', $msY);
                            $ms->addAttribute('in', '2');
                            $ms->addAttribute('out', '2');
                            $ms->addAttribute('c', '1');
                            $ms->addAttribute('name', self::TOOL_BUTT_GR['name']);
                            $ml = $this->xml->addChild('ml');
                            $ml->addAttribute('x', $mlX);
                            $ml->addAttribute('y', $mlY);
                        }
                    }
                } elseif ($rect['z'] == 0) {
                    switch (true) {
                        case $rect['x'] == 0 && $rect['y'] == 0:
                            if (!($rect['r'] > 0)) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] - 5,
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] - 5,
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                    ];
                            }
                            break;
                        case $rect['x'] == 0 && $rect['y'] > 0
                            && (float)$rect['y'] + (float)$rect['height'] == $this->detail['h']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] + 5,
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] + 5,
                                            ]
                                        ],
                                    ];
                            }

                            break;
                        case $rect['x'] == 0 && $rect['y'] > 0 && (float)$rect['y'] + (float)$rect['height'] < $this->detail['h']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['width'] - (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] - 5,
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] - 5,
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                    ];
                            }
                            break;
                        case $rect['y'] == 0 && $rect['x'] > 0 && (float)$rect['x'] + (float)$rect['width'] < $this->detail['l']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'] - 5,
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'] - 5,
                                            ]
                                        ],
                                    ];
                            }
                            break;
                        case $rect['y'] == 0 && $rect['x'] > 0 && (float)$rect['x'] + (float)$rect['width'] == $this->detail['l']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'] + (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['r'],
                                                'cy' => $rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'] - 5,
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'] - 5,
                                            ]
                                        ],
                                    ];
                            }
                            break;
                        case $rect['y'] > 0 && $rect['x'] > 0 && (float)$rect['y'] + (float)$rect['height'] < $this->detail['h']
                            && (float)$rect['x'] + (float)$rect['width'] == $this->detail['l']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'] + (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['x'] + (float)$rect['r'],
                                                'y' => $rect['y'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['r'],
                                                'cy' => $rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] + 5,
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] + 5,
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                    ];
                            }
                            break;
                        case $rect['y'] > 0 && $rect['x'] > 0 && (float)$rect['y'] + (float)$rect['height'] == $this->detail['h']
                            && (float)$rect['x'] + (float)$rect['width'] == $this->detail['l']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'] + (float)$rect['r'],
                                                'y' => $rect['y'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['r'],
                                                'cy' => $rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] + 5,
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] + 5,
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                    ];
                            }
                            break;
                        case $rect['y'] > 0 && $rect['x'] > 0 && (float)$rect['y'] + (float)$rect['height'] == $this->detail['h']
                            && (float)$rect['x'] + (float)$rect['width'] < $this->detail['l']:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'] + $rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['x'] + (float)$rect['r'],
                                                'y' => (float)$rect['y'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] + 5,
                                            ]
                                        ]
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] + 5,
                                            ]
                                        ]
                                    ];
                            }
                            break;
                        default:
                            if ($rect['r'] > 0) {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'] + (float)$rect['r'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                                'dir' => 'true',
                                                'cx' => (float)$rect['x'] + (float)$rect['width'] - (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['r'],
                                                'y' => (float)$rect['y'] + (float)$rect['height']
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => (float)$rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['height'] - (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                        [
                                            'mac' => [
                                                'x' => $rect['x'] + (float)$rect['r'],
                                                'y' => $rect['y'],
                                                'dir' => 'true',
                                                'cx' => $rect['x'] + (float)$rect['r'],
                                                'cy' => (float)$rect['y'] + (float)$rect['r'],
                                            ]
                                        ],
                                    ];
                            } else {
                                $coordinateArray =
                                    [
                                        [
                                            'ms' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => $rect['y'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => (float)$rect['x'] + (float)$rect['width'],
                                                'y' => (float)$rect['y'] + (float)$rect['height'],
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => (float)$rect['y'] + (float)$rect['height']
                                            ]
                                        ],
                                        [
                                            'ml' => [
                                                'x' => $rect['x'],
                                                'y' => $rect['y'],
                                            ]
                                        ]
                                    ];
                            }
                    }

                    foreach ($coordinateArray as $coordinate) {
                        foreach ($coordinate as $childName => $attributes) {
                            $child = $this->xml->addChild($childName);
                            if ($childName == 'ms') {
                                $child->addAttribute('dp', $this->detail['material']['thickness'] + 3);
                                $child->addAttribute('in', '202');
                                $child->addAttribute('out', '202');
                                $child->addAttribute('c', '1');
                                $child->addAttribute('name', 'Mill20');
                                $child->addAttribute('comment', $rect['comment'] ?? '');
                            }
                            foreach ($attributes as $attributeName => $attributeValue) {
                                $child->addAttribute($attributeName, $attributeValue);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getProgram(): object
    {
        return $this->xml;
    }
}