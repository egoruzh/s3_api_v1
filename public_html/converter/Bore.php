<?php

namespace Converter;

use ExtPHP\XmlToJson\XmlToJsonConverter;
use SimpleXMLElement;

class Bore
{
    private SimpleXMLElement $xml;

    public function __construct($xml)
    {
        $this->xml = $xml;
    }

    private function getBoreAttr(SimpleXMLElement $boreds, $boreName): array
    {
        foreach ($boreds->tool as $item) {
            if ((string)$item->attributes()->name === $boreName) {
                return (new XmlToJsonConverter($item))->toArray()['tool']['_attributes'] ?? [];
            }
        }
        return [];
    }

    private function getXY(array $programArray, array $params): array
    {
        if (isset($programArray['@attributes']['x'])) {
            $x = (string)$programArray['@attributes']['x'];
        } else {
            $x = 0.0;
        }
        if (isset($programArray['@attributes']['y'])) {
            $y = (string)$programArray['@attributes']['y'];
        } else {
            $y = 0.0;
        }

        $dx = (float)$params['attributes']['@attributes']['dx'];
        $dy = (float)$params['attributes']['@attributes']['dy'];

        $coordinates = [
        'x' => Calculator::getSumByString($x, $params['attributes']['@attributes']),
        'y' => Calculator::getSumByString($y, $params['attributes']['@attributes'])
        ];
        if (!$params['mirHor'] && $coordinates['x'] != 0) {
            $res['x'] = abs($coordinates['x']);
        } elseif ($coordinates['x'] != 0) {
            $res['x'] = abs($coordinates['x'] - $dx);
        } else {
            $res['x'] = $x;
        }
        if ($params['mirVert'] && $coordinates['y'] != 0) {
            $res['y'] = abs($coordinates['y']);
        } elseif ($coordinates['y'] != 0) {
            $res['y'] = abs($coordinates['y'] - $dy);
        } else {
            $res['y'] = $y;
        }
        $x = $coordinates['x'];
        $y = $coordinates['y'];

        $params['turn'] = $params['turn'] ?? 0;
        switch (true) {
            case $params['turn'] == 1:
                $res['y'] = $x;
                $res['x'] = $dy - $y;
                break;
            case $params['turn'] == 2:
                $res['x'] = $dx - $x;
                $res['y'] = $dy - $y;
                break;
            case $params['turn'] == 3:
                $res['x'] = $y;
                $res['y'] = $dx - $x;
                break;
        }
        return $res;
    }

    public function getBoredDetailByProgram(SimpleXMLElement $boredsProgram, array $params): array
    {
//        dd($boredsProgram->attributes(), $params);
        foreach ($boredsProgram as $operationTag => $program) {
            if (in_array($operationTag, ConvertProjectToJson::ALL_XML_SIDE_BY_BORE)) {
                $boreName = (string)$program->attributes()->name;
                $boreAC = (int)$program->attributes()->ac;
                $boreAS = (float)$program->attributes()->as;
                $boreAV = (string)$program->attributes()->av == 'true';
                $boreAttr = $this->getBoreAttr($boredsProgram, $boreName);
                $comment = (string)$program->attributes()->comment;
                $p = [
                    'attributes' => (array)$boredsProgram->attributes(),
                    'mirVert' => $params['mirVert'],
                    'mirHor' => $params['mirHor'],
                    'turn' => $params['turn'],
                    'operationName' => $operationTag,
                ];
                $xy = $this->getXY((array)$program, $p);

                $attributes = (array)$boredsProgram->attributes();
                $z = (float)preg_replace('/[^0-9.]/', '', (string)$program->attributes()->z);
                if ($z == 0) {
                    if (in_array($operationTag, ['bl', 'br', 'bt', 'bb'])) {
                        $z = round(((float)$attributes['@attributes']['dz'] / 2), 2);
                    }
                }
                $side = $this->getSideBore($operationTag, $params);
                $zRes = in_array($operationTag, ['bl', 'br', 'bt', 'bb']) ? (string)(abs(
                $z - ($params['side'] ? (float)$attributes['@attributes']['dz'] : 0)
                )) : "";

                $depthRes = (string)Calculator::getSumByString(
                (string)$program->attributes()->dp,
                $attributes['@attributes']
                );
                $diamRes = (string)Calculator::getSumByString((string)$boreAttr['d'], $attributes['@attributes']);
                for ($i = 0; $i < $boreAC; ++$i) {
                    $xPlus = $boreAV ? 0 : $i * $boreAS;
                    $yPlus = $boreAV ? $i * $boreAS : 0;
                    $xRes = $xy['x'] + $xPlus;
                    $yRes = $xy['y'] + $yPlus;
                    $res[] = [
                    'side' => $side,
                    'x' => (string)$xRes,
                    'y' => (string)$yRes,
                    'z' => $zRes,
                    'depth' => $depthRes,
                    'diam' => $diamRes,
                    'x_axis' => 'left',
                    'y_axis' => 'bottom',
                    'comment' => $comment ?? '',
                    ];
                }
            }
        }
        return $res ?? [];
    }

    public function getBoredPartId(int $partId): array
    {
        $result = [];
        foreach ($this->xml->operation as $operation) {
            foreach ($operation->part as $p) {
                if (((int)$p->attributes()->id === $partId) && (int)$operation->attributes()->countBore > 0) {
                    $result[] = [
                    'mirVert' => (string)$operation->attributes()->mirVert === "true",
                    'mirHor' => (string)$operation->attributes()->mirHor === "true",
                    'countBore' => (int)$operation->attributes()->countBore,
                    'side' => (string)$operation->attributes()->side === "true",
                    'turn' => (string)$operation->attributes()->turn,
                    'program' => $operation->attributes()->program,
                    ];
                }
            }
        }
        return $result;
    }

    public function getBoredByPartId($boredsPartId): array
    {
        $boredParts = $this->getBoredPartId((int)$boredsPartId);
        $count = 0;
        $boreds = [];
        foreach ($boredParts as $bored) {
            $program = $bored['program'];
            if ($bored['countBore'] > 0) {
                $count += $bored['countBore'];
                array_push(
                $boreds,
                ...$this->getBoredDetailByProgram(simplexml_load_string($program), $bored)
                );
            }
        }
        return [
        'countBore' => $count,
        'boreds' => $boreds,
        ];
    }

    private function getSideBore(string $operationTag, array $params): string
    {
        return match ($operationTag) {
            'bl' => $params['mirHor'] ? 'right' : 'left',
            'br' => $params['mirHor'] ? 'left' : 'right',
            'bt' => $params['mirVert'] ? 'bottom' : 'top',
            'bb' => $params['mirVert'] ? 'top' : 'bottom',
            default => $params['side'] ? 'front' : 'back',
        };
    }
}
