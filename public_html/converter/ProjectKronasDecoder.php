<?php

namespace Converter;

use Exception;

class ProjectKronasDecoder
{
    /**
     * @throws Exception
     */
    public static function decode(string $base64): string|array
    {
        $str = strrev(base64_decode($base64));
        $array = unserialize($str);
        if(!isset($array['project_data'])){
            throw new Exception('not valid kronas file');
        }
        return str_replace("\'","'", $array['project_data']);;
    }

    public static function encode(string $xml): string
    {
        $array = [
            'user_type_edge' => 'eva',
            'user_place' => '1',
            'project_data' => self::clearString($xml),
        ];
        $strRev = strrev(serialize($array));

        return base64_encode($strRev);
    }

    private static function clearString(string $str): string
    {
        $clean = trim($str, "\x00..\x1F");
        $replace = array('<?xml version="1.0" encoding="UTF-8"?>', "\r", "\n", "\t");
        return str_replace($replace, "", $clean);
    }
}