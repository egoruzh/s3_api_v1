<?php

namespace Converter;

class Corner
{
    private const CORNER_LEFT_BOTTOM = 'left_bottom';
    private const CORNER_LEFT_TOP = 'left_top';
    private const CORNER_RIGHT_TOP = 'right_top';
    private const CORNER_RIGHT_BOTTOM = 'right_bottom';

    public static function isConnerCutLine(array $line, $params): bool|array
    {
        if ($params['dz'] > $line['dp']) {
            return false;
        }

        $x1Corner = $line['x1'] == 0 || $line['x1'] == $params['dx'];
        $y1Corner = $line['y1'] == 0 || $line['y1'] == $params['dy'];
        $x2Corner = $line['x2'] == 0 || $line['x2'] == $params['dx'];
        $y2Corner = $line['y2'] == 0 || $line['y2'] == $params['dy'];

        if ($x1Corner == $x2Corner || $y1Corner == $y2Corner) {
            //todo: от угла на противоположную сторону тоже угол
            return false;
        }
        if ($line['name'] == 'mac') {
            $result['type'] = 'radius';
            $result['r'] = abs($line['x2'] - $line['x1']);
        } else {
            $result['type'] = 'line';
            $result['r'] = '';
        }
        $x1 = $line['x1'];
        $x2 = $line['x2'];
        $y1 = $line['y1'];
        $y2 = $line['y2'];
        $params['turn'] = $params['turn'] ?? 0;
        switch (true) {
            case $params['turn'] == 1:
                $line['y1'] = $x1;
                $line['y2'] = $x2;
                $line['x1'] = $params['dy'] - $y1;
                $line['x2'] = $params['dy'] - $y2;
                break;
            case $params['turn'] == 2:
                $line['x1'] = $params['dx'] - $x1;
                $line['x2'] = $params['dx'] - $x2;
                $line['y1'] = $params['dy'] - $y1;
                $line['y2'] =  $params['dy'] - $y2;
                break;
            case $params['turn'] == 3:
                $line['x1'] = $y1;
                $line['x2'] = $y2;
                $line['y1'] = $params['dx'] - $x1;
                $line['y2'] = $params['dx'] - $x2;
                break;
        }
        $x1Corner = $line['x1'] == 0 || $line['x1'] == $params['dx'];
        $y1Corner = $line['y1'] == 0 || $line['y1'] == $params['dy'];

        $result['x'] = $x1Corner ? $line['x2'] : $line['x1'];
        $result['y'] = $y1Corner ? $line['y2'] : $line['y1'];

        $result['angle'] = self::getCornerSide($line['x1'], $line['x2'], $line['y1'], $line['y2']);
        $result['comment'] = $line['comment'] ?? '';

        switch (true){
            case $result['type'] == 'line' && $result['angle'] == self::CORNER_RIGHT_TOP:
                $result['x'] = $params['dx'] - $result['x'];
                $result['y'] = $params['dy'] - $result['y'];
                break;
            case $result['type'] == 'line' && $result['angle'] == self::CORNER_RIGHT_BOTTOM:
                $result['x'] = $params['dx'] - $result['x'];
                break;
            case $result['type'] == 'line' && $result['angle'] == self::CORNER_LEFT_TOP:
                $result['y'] = $params['dy'] - $result['y'];

        }
        return $result;
    }

    public static function getCornerSide($x1, $x2, $y1, $y2): bool|string
    {
        $left = $x1 == 0 || $x2 == 0;
        $bottom = $y1 == 0 || $y2 == 0;

        return match (true) {
            $left && $bottom => self::CORNER_LEFT_BOTTOM,
            $left && !$bottom => self::CORNER_LEFT_TOP,
            !$left && !$bottom => self::CORNER_RIGHT_TOP,
            !$left && $bottom => self::CORNER_RIGHT_BOTTOM,
            default => false,
        };
    }

    public function getCornerByLines(array &$lines, array $params): array
    {
        $result = [];
        foreach ($lines as $k => $line) {
            $corner = self::isConnerCutLine($line, $params);
            if ($corner) {
                $result[] = $corner;
                unset($lines[$k]);
            }
        }
        return $result;
    }
}
