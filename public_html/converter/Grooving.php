<?php

namespace Converter;

use SimpleXMLElement;

class Grooving
{
    private static function getLeftBottomCoordinate(array $operationAttributes, array $params): bool|array
    {
        if ($params['mirVert']) {
            $result['y1'] = $operationAttributes['y1'];
            $result['y2'] = $operationAttributes['y2'];
        } else {
            $result['y1'] = abs($operationAttributes['y1'] - $params['dy']);
            $result['y2'] = abs($operationAttributes['y2'] - $params['dy']);
        }
        if ($params['mirHor']) {
            $result['x1'] = abs($operationAttributes['x1'] - $params['dx']);
            $result['x2'] = abs($operationAttributes['x2'] - $params['dx']);
        } else {
            $result['x1'] = $operationAttributes['x1'];
            $result['x2'] = $operationAttributes['x2'];
        }
        $result['dp'] = $operationAttributes['dp'];
        $result['t'] = $operationAttributes['t'];
        $result['correction'] = match (true) {
            $operationAttributes['c'] == 0 => 'center',
            $operationAttributes['c'] == 1 => 'right',
            $operationAttributes['c'] == 2 => 'left',
            $operationAttributes['c'] == 3 => 'pocket',
            default => false,
        };
        $result['comment'] = $operationAttributes['comment'] ?? '';
        return $result;
    }

    /**
     * @throws ConverterException
     */
    private function getGroovingFormated(array $coordinate, $params): array
    {
        if (($coordinate['x1'] != $coordinate['x2']) && ($coordinate['y1'] != $coordinate['y2'])) {
            throw new ConverterException(
                'format not supported. Пазование не паралельно к осям' . json_encode($coordinate, JSON_PRETTY_PRINT)
            );
        }
        $width = ($coordinate['x1'] == $coordinate['x2'] ? $coordinate['t'] : abs(
            $coordinate['x1'] - $coordinate['x2']
        ));
        $height = ($coordinate['y1'] == $coordinate['y2'] ? $coordinate['t'] : abs(
            $coordinate['y1'] - $coordinate['y2']
        ));

        $x = ($coordinate['x1'] == $coordinate['x2'] && $coordinate['correction'] == 'right') ? $coordinate['x1'] - $width : min(
            $coordinate['x1'],
            $coordinate['x2']
        );
        $y = ($coordinate['y1'] == $coordinate['y2'] && $coordinate['correction'] == 'left') ? $coordinate['y1'] - $height : min(
            $coordinate['y1'],
            $coordinate['y2']
        );
        if ($coordinate['correction'] == 'center') {
            switch (true) {
                case $coordinate['x1'] == $coordinate['x2']:
                    $x -= $coordinate['t'] / 2;
                    break;
                case $coordinate['y1'] == $coordinate['y2']:
                    $y -= $coordinate['t'] / 2;
                    break;
            }
        }
        $result['side'] = $params['side'] ? 'front' : 'back';
        $result['x'] = (string)$x;
        $result['y'] = (string)$y;
        $result['z'] = 0;
        $result['width'] = (string)$width;
        $result['height'] = (string)$height;
        $result['depth'] = (string)$coordinate['dp'];
        $result['r'] = 0;
        $result['edge'] = '';
        $result['ext'] = false;
        $result['comment'] = $coordinate['comment'] ?? '';

        return $result;
    }

    private static function round(array $coordinate, array $params): array
    {
        $vars = ['dx' => $params['dx'], 'dy' => $params['dy'], 'dz' => $params['dz']];
        $coordinate['x1'] = Calculator::getSumByString((string)$coordinate['x1'], $vars);
        $coordinate['x2'] = Calculator::getSumByString((string)$coordinate['x2'], $vars);
        $coordinate['y1'] = Calculator::getSumByString((string)$coordinate['y1'], $vars);
        $coordinate['y2'] = Calculator::getSumByString((string)$coordinate['y2'], $vars);

        if ($coordinate['x1'] < 0) {
            $coordinate['x1'] = 0;
        }
        if ($coordinate['x1'] > $params['dx']) {
            $coordinate['x1'] = $params['dx'];
        }

        if ($coordinate['x2'] < 0) {
            $coordinate['x2'] = 0;
        }
        if ($coordinate['x2'] > $params['dx']) {
            $coordinate['x2'] = $params['dx'];
        }

        if ($coordinate['y1'] < 0) {
            $coordinate['y1'] = 0;
        }
        if ($coordinate['y1'] > $params['dy']) {
            $coordinate['y1'] = $params['dy'];
        }

        if ($coordinate['y2'] < 0) {
            $coordinate['y2'] = 0;
        }
        if ($coordinate['y2'] > $params['dy']) {
            $coordinate['y2'] = $params['dy'];
        }
        $x1 = $coordinate['x1'];
        $x2 = $coordinate['x2'];
        $y1 = $coordinate['y1'];
        $y2 = $coordinate['y2'];
        $params['turn'] = $params['turn'] ?? 0;
        switch (true) {
            case $params['turn'] == 1:
                $coordinate['y1'] = $x1;
                $coordinate['y2'] = $x2;
                $coordinate['x1'] = $params['dy'] - $y1;
                $coordinate['x2'] = $params['dy'] - $y2;
                break;
            case $params['turn'] == 2:
                $coordinate['x1'] = $params['dx'] - $x1;
                $coordinate['x2'] = $params['dx'] - $x2;
                $coordinate['y1'] = $params['dy'] - $y1;
                $coordinate['y2'] = $params['dy'] - $y2;
                break;
            case $params['turn'] == 3:
                $coordinate['x1'] = $y1;
                $coordinate['x2'] = $y2;
                $coordinate['y1'] = $params['dx'] - $x1;
                $coordinate['y2'] = $params['dx'] - $x2;
                break;
        }
        return $coordinate;
    }

    /**
     * @throws ConverterException
     */
    public function getGroovingByProgram(SimpleXMLElement $program, array $params): array
    {
        $result = [];

        foreach ($program as $oName => $operation) {
            if ($oName !== 'gr') {
                continue;
            }
            $operationArray = (array)$operation;
            $operationAttributes = $operationArray['@attributes'];
            //<gr x1="386" y1="0" dp="9" x2="386" y2="600" t="4" c="1" name="Cut4"/>
            $roundCoordinate = self::round($operationAttributes, $params);
            $groovingCoordinates = self::getLeftBottomCoordinate($roundCoordinate, $params);
            $groovingResult = self::getGroovingFormated($groovingCoordinates, $params);
            $result[] = $groovingResult;
        }
        return $result;
    }
}