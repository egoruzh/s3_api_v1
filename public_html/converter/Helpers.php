<?php

namespace Converter;

use ZipArchive;

class Helpers
{
    const TMP_DIR = '/files/tmp/';
    const PUBLIC_DIR = '/files/giblab/';
    const RESULT_GIBLAB_PROJECT = 'result.project';
    static string $uniqName;

    public static function listDirectory($dir): array
    {
        $file_list = [];
        $stack[] = $dir;

        while ($stack) {
            $current_dir = array_pop($stack);
            if ($dh = opendir($current_dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file !== '.' and $file !== '..') {
                        $current_file = "{$current_dir}/{$file}";
                        if (is_file($current_file)) {
                            $file_list[] = "{$current_dir}/{$file}";
                        } elseif (is_dir($current_file)) {
                            $stack[] = $current_file;
                            $file_list[] = "{$current_dir}/{$file}/";
                        }
                    }
                }
            }
        }

        return $file_list;
    }

    private static function publicDir(): string
    {
        $dir = realpath('../') . self::PUBLIC_DIR . self::$uniqName . '/';
        mkdir($dir);
        return $dir;
    }

    private static function tmpDir(): string
    {
        $dir = realpath('../') . self::TMP_DIR . self::$uniqName . '/';
        mkdir($dir);
        return $dir;
    }

    public static function delTree(string $dir): bool
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }

        return rmdir($dir);
    }

    public static function buildLink(string $fileAbsolutePath): string
    {
        $fileRealPath = realpath($fileAbsolutePath);
        if (PHP_SAPI === 'cli') {
            return $fileAbsolutePath;
        }
        $cutString = realpath($_SERVER['DOCUMENT_ROOT']);

        if (str_starts_with($fileRealPath, $cutString)) {
            $fileRealPath = substr($fileRealPath, strlen($cutString));
        }

        $protokol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';
        $homeUrl = $protokol . '://' . $_SERVER['HTTP_HOST'];
        $filePath = str_replace('\\', '/', $fileRealPath);

        return $homeUrl . $filePath;
    }

    /**
     * @param string $dataZip
     * @param bool $returnContent
     * @return array|false|string
     */
    public static function extractZip(string $dataZip, bool $returnContent = false)
    {
        self::$uniqName = preg_replace('/\D/', '', microtime(1));
        $result = [];
        $tmpDir = self::tmpDir();
        $extractDir = self::publicDir();
        $zipFileRoot = $tmpDir . 'file.zip';
        file_put_contents($zipFileRoot, $dataZip);
        $zip = new ZipArchive;
        $res = $zip->open($zipFileRoot);
        if ($res === true) {
            $zip->extractTo($extractDir);
            $zip->close();
        } else {
            return false;
        }
        //Helpers::delTree($tmpDir);
        if ($returnContent) {
            return is_file($extractDir . self::RESULT_GIBLAB_PROJECT) ?
                file_get_contents($extractDir . self::RESULT_GIBLAB_PROJECT) : false;
        }
        $extractFiles = self::listDirectory($extractDir);
        foreach ($extractFiles as $file) {
            $result[] = self::buildLink($file);
        }

        return $result;
    }

    public static function isXml(string $value): bool
    {
        $prev = libxml_use_internal_errors(true);

        $doc = simplexml_load_string($value);
        $errors = libxml_get_errors();

        libxml_clear_errors();
        libxml_use_internal_errors($prev);

        return false !== $doc && empty($errors);
    }
}