<?php

namespace Converter;

use SimpleXMLElement;

class GroovingProgram
{
    private \SimpleXMLElement $xml;
    public const ALL_GR_ATTRIBUTES = [
        'grts' => 'grt',
        'grrs' => 'grr',
        'grbs' => 'grb',
        'grls' => 'grl',
    ];

    public function __construct(\SimpleXMLElement $xml)
    {
        $this->xml = $xml;
    }

    private static function getRectFormated(string $grType, array $data, string $side): array
    {
        $result = [];
        $x = 0;
        $y = 0;
        $width = 0;
        $height = 0;
        switch (true) {
            case $grType == 'grt' || $grType == 'grb':
                $width = $data['partAttr']['dl'];
                $height = $data['grAttr']['grWidth'];
                if ($grType == 'grt') {
                    $grOffset = (float)$data['partAttr']['dw'] - (float)$data['grAttr']['grOffset'];
                    $y = $data['grAttr']['grOffsetIncl'] == 'true' ? $grOffset : $grOffset - $data['grAttr']['grWidth'];
                } else {
                    $grOffset = (float)$data['grAttr']['grOffset'];
                    $y = $data['grAttr']['grOffsetIncl'] == 'true' ? $grOffset - $data['grAttr']['grWidth'] : $grOffset;
                }
                break;
            case $grType == 'grr' || $grType == 'grl':
                $height = $data['partAttr']['dw'];
                $width = $data['grAttr']['grWidth'];
                if ($grType == 'grr') {
                    $grOffset = (float)$data['partAttr']['dl'] - (float)$data['grAttr']['grOffset'];
                    $x = $data['grAttr']['grOffsetIncl'] == 'true' ? $grOffset : $grOffset - $data['grAttr']['grWidth'];
                } else {
                    $grOffset = (float)$data['grAttr']['grOffset'];
                    $x = $data['grAttr']['grOffsetIncl'] == 'true' ? $grOffset - $data['grAttr']['grWidth'] : $grOffset;
                }
                break;
        }
        if ($x < 0 && ($grType == 'grr' || $grType == 'grl')) {
            $width = $width + $x;
            $x = 0;
        } elseif ($x + $width > $data['partAttr']['dl']) {
            $width = $data['partAttr']['dl'] - $x;
            $x = $data['partAttr']['dl'] - $width;
        }
        if ($y < 0 && ($grType == 'grt' || $grType == 'grb')) {
            $height = $height + $y;
            $y = 0;
        } elseif ($y + $height > $data['partAttr']['dw']) {
            $height = $data['partAttr']['dw'] - $y;
        }

        $result['side'] = $side;
        $result['x'] = (string)$x;
        $result['y'] = (string)$y;
        $result['z'] = 0;
        $result['width'] = (string)$width;
        $result['height'] = (string)$height;
        $result['depth'] = (string)$data['grAttr']['grDepth'];
        $result['r'] = 0;
        $result['edge'] = null;
        $result['ext'] = false;
        return $result;
    }

    private static function xmlAttributeToArray(SimpleXMLElement $nodes): array
    {
        $result = [];
        foreach ($nodes->attributes() as $name => $attribute) {
            $result[$name] = (string)$attribute;
        }
        return $result;
    }

    public function getRectsByPartId(int $partId): array
    {
        $result = [];
        $part = $this->xml->xpath("//good[@typeId='product']/part[@id=$partId]");
        $part = $part[0];
        $partAttributesArray = self::xmlAttributeToArray($part);
        foreach (self::ALL_GR_ATTRIBUTES as $sideAttr => $grType) {
            $partGr = $part->attributes()->$grType;
            if (!$partGr) {
                continue;
            }

            $operationId = explode('#', $partGr)[1];
            $operation = $this->xml->xpath("//operation[@typeId='GR' and @id=$operationId]");
            $operationAttributesArray = self::xmlAttributeToArray($operation[0]);
            $data = [
                'grAttr' => $operationAttributesArray,
                'partAttr' => $partAttributesArray,
                'side' => (string)$part->attributes()->$sideAttr == 'false' ? 'back' : 'front',
            ];
            $side = (string)$part->attributes()->$sideAttr == 'false' ? 'back' : 'front';
            $result[] = self::getRectFormated($grType, $data, $side);
        }

        return $result;
    }


}