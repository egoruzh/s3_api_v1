<?php

namespace Converter;

class Smile
{

    private static function getSmileGabarites(array $coordinate, $params): array
    {
        $countNames = array_count_values(array_column($coordinate, 'name'));
        $r1 = $coordinate[0];
        $r2 = $coordinate[1];
        if ($r1['cx'] == $r1['x1']) {
            $radius1 = abs($r1['cy'] - $r1['y1']);
        } else {
            $radius1 = abs($r1['cx'] - $r1['x1']);
        }
        $tx1 = $r2['cx'];
        $ty1 = $r2['cy'];
        $tx2 = $r2['x2'];
        $ty2 = $r2['y2'];
        $radius2 = sqrt((($tx2 - $tx1) * ($tx2 - $tx1)) + (($ty2 - $ty1) * ($ty2 - $ty1)));
        $result['r1'] = round($radius1, 3);
        $result['r2'] = round($radius2, 3);

        return $result;
    }

    private static function validateFigure(array $groupLine, array $params): bool
    {
        $groupLine = array_values($groupLine);
        $linaNames = array_column($groupLine, 'name');
        $countLines = array_count_values($linaNames);
        $cMac = $countLines['mac'] ?? 0;
        $cMl = $countLines['ml'] ?? 0;

        if ($cMac == 3 && $cMl == 0) {
            return true;
        }
        if ($cMac == 4 && ($cMl == 1 || $cMl == 0)) {
            return true;
        }

        return false;
    }

    public function getSmileByLines(array $groupLines, array $params): array
    {
        foreach ($groupLines as $groupLine) {
            $v = self::validateFigure($groupLine, $params);
            if (!$v) {
                continue;
            }
            $result[] = self::getSmileFormated($groupLine, $params);
        }

        return $result ?? [];
    }

    private static function getSmileFormated(array $coordinate, $params): array
    {
        $mac = false;
        $ml = false;
        $coordinate = array_values($coordinate);

        $changex2y2 = [];

        foreach ($coordinate as $cc) {
            $x[] = $cc['x1'];
            $x[] = $cc['x2'];
            $y[] = $cc['y1'];
            $y[] = $cc['y2'];
            if ($cc['name'] == 'mac') {
                $mac = $cc;
            }
            if ($cc['name'] == 'ml') {
                $ml = true;
            }
        }

        $coordinate = array_values($coordinate);
        if (isset($coordinate[2]) && $coordinate[2]['name'] == 'ml') {
            $line = max(
                abs($coordinate[2]['x1'] - $coordinate[2]['x2']),
                abs($coordinate[2]['y1'] - $coordinate[2]['y2'])
            );
        } else {
            $line = '';
        }

        $minX = min($x);
        $minY = min($y);
        $maxX = max($x);
        $maxY = max($y);


        switch (true) {
            case $minX == 0:
            case $minY == 0:
            case $maxX == $params['dx']:
            case $maxY == $params['dy']:
                $ext = true;
                break;
        }
        $radius = $mac ? abs($mac['x2'] - $mac['x1']) : '';
        $ltrb = match (true) {
            $minY == 0 => 'bottom',
            $maxY == $params['dy'] => 'top',
            $maxX == $params['dx'] => 'right',
            default => 'left',
        };

        $smileGabarite = self::getSmileGabarites($coordinate, $params);

        switch (true) {
            case $ltrb == 'right':
                $width = abs($params['dx'] - $minX);
                $height = abs($maxY - $minY);
                break;
            case $ltrb == 'left':
                $width = abs($params['dx'] - $maxX);
                $height = abs($maxY - $minY);
                break;
            case $ltrb == 'top':
                $width = abs($maxX - $minX);
                $height = abs($params['dy'] - $minY);
                break;
            default:
                $width = abs($maxX - $minX);
                $height = $maxY;
        }
        $result['side'] = $params['side'] ? 'front_' . $ltrb : 'back_' . $ltrb;
        $result['x'] = (string)$minX;
        $result['y'] = (string)$minY;
        $result['width'] = (string)($width);
//        $result['height'] = (string)($height);
        $result['r1'] = (string)$smileGabarite['r1'];
        $result['r2'] = (string)$smileGabarite['r2'];
        $result['lineTop'] = (string)$line;
        $result['edge'] = '';


        return $result;
    }
}