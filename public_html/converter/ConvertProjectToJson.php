<?php

namespace Converter;

use App\Http\Controllers\Api\V1\Converter;
use Exception;
use ExtPHP\XmlToJson\XmlToJsonConverter;
use SimpleXMLElement;

class ConvertProjectToJson

{
    const PARAMS = ['mirVert', 'mirHor', 'typeId', 'side', 'id', 'countBore', 'turn'];
    const SEARCH_ATTR_MATERIAL = 'code'; //
    private Bore $bore;
    private Rect $rect;
    private ButtGroove $buttGroove;
    private Smile $smile;
    private Corner $corner;
    private Grooving $grooving;
    private GroovingProgram $groovingProgram;
    public const DIRECTOIN = [
        'ell' => 'left',
        'elr' => 'right',
        'elt' => 'top',
        'elb' => 'bottom',
    ];
    public const ALL_XML_SIDE_BY_BORE = [
        'bf',
        'bl',
        'br',
        'bt',
        'bb',
    ];
    public const EXPORT_GOOD = [
        'id' => 'id',
        'code' => 'article',
        't' => 'thickness',
        'w' => 'width',
        'name' => 'name',
    ];
    public const EXPORT_MATERIAL = [
        'w' => 'width',
        'l' => 'height',
        't' => 'thickness',
        'code' => 'article',
        'name' => 'name',
        'id' => 'id',
        'material_id' => 'material_id',
    ];

    private SimpleXMLElement $xml;

    private array $projectArray;
    private array $edges = [];
    private array $materials;
    /**
     * @var false
     */
    public bool $status = true;
    public string $message = '';

    /**
     * @throws Exception
     */
    public function __construct(string $xmlStr, $recalculate = false)
    {
        $check = Helpers::isXml($xmlStr);
        if (!$check) {
            $this->message = 'not valid xml';
            $this->status = false;
        } else {
            $xmlStr = $recalculate ? Giblab::getResultFromXML('recalculate', $xmlStr, true) : $xmlStr;
            $this->xml = simplexml_load_string($xmlStr);
            $this->parseEdgesMaterial();
            $this->cleanEdges();
            $this->parseSheetsMaterial();
            $this->projectArray = (new XmlToJsonConverter($this->xml))->toArray();
            $this->bore = new Bore($this->xml);
            $this->rect = new Rect();
            $this->corner = new Corner();
            $this->grooving = new Grooving();
            $this->groovingProgram = new GroovingProgram($this->xml);
            $this->buttGroove = new ButtGroove();
            $this->smile = new Smile();
        }
    }

    private static function explodeFigure(array $figure, array $perimeter, array $params): bool|array
    {
        $explodeLines = [];
        $sFigure = $perimeter['gabarites']['w'] * $perimeter['gabarites']['h'];
        $s = 0;
        foreach ($figure as $line) {
            $direction = $line['directionLine'] == 'toLeft' || $line['directionLine'] == 'toRight' ? 'ver' : 'hor';
            $yAll[] = $line['y1'];
            $yAll[] = $line['y2'];
            $xAll[] = $line['x1'];
            $xAll[] = $line['x2'];
            $lineDirectionGroup[$direction][] = $line;
            if ($line['x1'] == $line['x2']) {
                $s += abs($line['y1'] - $line['y2']) * (float)$line['d'];
            } elseif ($line['y1'] == $line['y2']) {
                $s += abs($line['x1'] - $line['x2']) * (float)$line['d'];
            }
            $explodeLines[] = [$line];
        }
//        if ($s < $sFigure && $figure && $figure[0]['dp'] < $params['dz']){
//            $position = abs(min($yAll) - max($yAll)) <= abs(min($xAll) - max($xAll)) ? 'hor' : 'ver';
//            if($position == 'hor'){
//
//                $xyUniq = [
//                    'yUniq' => array_unique($yAll),'xUniq' => array_unique($xAll)
//                ];
////                foreach ($xyUniq as )
////                dd($sss);
//            }
//            dd($figure, $lineDirectionGroup, min($yAll), max($yAll), min($xAll),max($xAll), $position);
//        }
//            return false;
        return ($s < $sFigure && $figure && $figure[0]['dp'] < $params['dz']) ? $explodeLines : false;
    }

    private static function mergeLineIntersect(array $figure, array $perimeter, array $params): array
    {
        foreach ($figure as $key => $line) {
            $lineNext = isset($figure[$key + 1]) ? $figure[$key + 1] : false;
            $chInter = self::isLineInFigure($line, $perimeter, $lineNext, $params);
            if ($chInter) {
                unset($figure[$key]);
            }
        }
        $copyFigure = $figure;

        foreach ($figure as $key => &$line) {
            foreach ($copyFigure as $key2 => $f) {
                if ($key == $key2) {
                    continue;
                }
//                if ($line['name'] == 'mac' && $f['name'] == 'mac' && ($key2 - $key == 1)) {
//                    if ($line['cx'] == $f['cx'] && $line['cy'] == $f['cy']) {
//                        $line['y2'] = $f['y2'];
//                        $line['x2'] = $f['x2'];
//                        unset($figure[$key2]);
//                        break(2);
//                    }
//                }
//                if ($line['x1'] == $f['x1'] && $line['x1'] == $f['x2'] && $line['y1'] == $f['y1'] && $line['y2'] == $f['y2'] && $line['name'] == 'mac') {
//                    $line['dp'] = max($f['dp'], $line['dp']);
//
//                    unset($figure[$key2]);
//                    continue;
//                }
//                dd($figure);
                $intersect = Coordinate::intersect(
                    $line['x1'],
                    $line['y1'],
                    $line['x2'],
                    $line['y2'],
                    $f['x1'],
                    $f['y1'],
                    $f['x2'],
                    $f['y2']
                );

                $aAndBonX = $line['x1'] == $line['x2'] && $f['x1'] == $f['x2'] && $line['x1'] == $f['x1'];
                $aAndBonY = $line['y1'] == $line['y2'] && $f['y1'] == $f['y2'] && $line['y1'] == $f['y1'];
                if ($intersect && ($aAndBonX || $aAndBonY) && $line['dp'] >= $params['dz']) {
                    switch (true) {
                        case $line['directionLine'] == 'toLeft':
                            $line['x1'] = max($line['x1'], $line['x2'], $f['x1'], $f['x2']);
                            $line['x2'] = min($line['x1'], $line['x2'], $f['x1'], $f['x2']);
                            break;
                        case $line['directionLine'] == 'toRight':
                            $line['x1'] = min($line['x1'], $line['x2'], $f['x1'], $f['x2']);
                            $line['x2'] = max($line['x1'], $line['x2'], $f['x1'], $f['x2']);
                            break;
                        case $line['directionLine'] == 'toBottom':
                            $line['y1'] = max($line['y1'], $line['y2'], $f['y1'], $f['y2']);
                            $line['y2'] = min($line['y1'], $line['y2'], $f['y1'], $f['y2']);
                            break;
                        case $line['directionLine'] == 'toTop':
                            $line['y1'] = min($line['y1'], $line['y2'], $f['y1'], $f['y2']);
                            $line['y2'] = max($line['y1'], $line['y2'], $f['y1'], $f['y2']);
                            break;
                    }
                    unset($figure[$key2]);
                }
            }
        }

        return $figure;
    }

    private static function getPerimeter(mixed $figure): array
    {
        $x = $y = $lineNames = [];

        foreach ($figure as $line) {
            $k = (string)preg_replace('/\D/', '', $line['x1']);
            $x[$k] = $line['x1'];
            $k = (string)preg_replace('/\D/', '', $line['x2']);
            $x[$k] = $line['x2'];
            $k = (string)preg_replace('/\D/', '', $line['y1']);
            $y[$k] = $line['y1'];
            $k = (string)preg_replace('/\D/', '', $line['y2']);
            $y[$k] = $line['y2'];
            $lineNames[] = $line['name'];
            $x2[] = $line['x1'];
            $x2[] = $line['x2'];
            $y2[] = $line['y1'];
            $y2[] = $line['y2'];
        }
        $gabarites = ['x' => min($x2), 'y' => min($y2), 'w' => max($x2) - min($x2), 'h' => max($y2) - min($y2)];

        return [
            'minX' => min($x),
            'maxX' => max($x),
            'minY' => min($y),
            'maxY' => max($y),
            'countLineNames' => array_count_values($lineNames),
            'gabarites' => $gabarites,
        ];
    }

    private static function isLineInFigure(array $line, array $perimeter, $lineNext, $params): bool
    {
        $aOnPerimeter = ($line['y1'] == $perimeter['minY'] || $line['y1'] == $perimeter['maxY']) || ($line['x1'] == $perimeter['minX'] || $line['x1'] == $perimeter['maxX']);
        $bOnPerimeter = ($line['y2'] == $perimeter['minY'] || $line['y2'] == $perimeter['maxY']) || ($line['x2'] == $perimeter['minX'] || $line['x2'] == $perimeter['maxX']);
        if ((!$aOnPerimeter || !$bOnPerimeter) && $params['dz'] < $line['dp']) {
            if ($line['name'] == 'ml') {
                return true;
            }
            if ($line['name'] == 'mac' && isset($perimeter['countLineNames']['ml']) && $perimeter['countLineNames']['ml'] > 1) {
                return true;
            }
        }
        if ((!$aOnPerimeter || !$bOnPerimeter) && $line['name'] == 'mac') {
            if ($lineNext && $lineNext['name'] == 'mac' && ($lineNext['x1'] == $line['x2'] && $lineNext['y1'] == $line['y2'])) {
                return false;
            }
            return true;
        }
        return false;
    }

    private static function fixIfOutCutNew(array $lines, array $params): array
    {
        $result = [];
        $group = 0;
        foreach ($lines as $k => $line) {
            $continue = false;
            $correct = match (true) {
                $line['cutterLine'] == 'right', $line['cutterLine'] == 'left' => (float)$line['d'],
                $line['cutterLine'] == 'center' => (float)$line['d'] / 2,
                default => 0,
            };
            $next = $lines[$k + 1] ?? null;
            if (!empty($next) && ($line['x2'] == $next['x1']) && ($line['y2'] == $next['y1']) && $params['dz'] > $line['dp']) {
                if ($line['cutterLine'] == 'right') {
                    if ($line['directionLine'] == 'toRight' && $next['directionLine'] == 'toTop' ||
                        $line['directionLine'] == 'toTop' && $next['directionLine'] == 'toLeft' ||
                        $line['directionLine'] == 'toLeft' && $next['directionLine'] == 'toBottom' ||
                        $line['directionLine'] == 'toBottom' && $next['directionLine'] == 'toRight'
                    ) {
                        switch (true) {
                            case $line['directionLine'] == 'toRight':
                                $lines[$k]['x2'] = min($line['x2'] + $correct, $params['dx']);
                                $lines[$k + 1]['y1'] = max($next['y1'] - $correct, 0);
                                break;
                            case $line['directionLine'] == 'toTop':
                                $lines[$k]['y2'] = min($line['y2'] + $correct, $params['dy']);
                                $lines[$k + 1]['x1'] = min($next['x1'] + $correct, $params['dx']);
                                break;
                            case $line['directionLine'] == 'toLeft':
                                $lines[$k]['x2'] = max($line['x2'] - $correct, 0);
                                $lines[$k + 1]['y1'] = min($next['y1'] + $correct, $params['dy']);
                                break;
                            case $line['directionLine'] == 'toBottom':
                                $lines[$k]['y2'] = max($line['y2'] - $correct, 0);
                                $lines[$k + 1]['x1'] = max($next['x1'] - $correct, 0);
                                break;
                        }
                    }
                }
                if ($line['cutterLine'] == 'left') {
                    if ($line['directionLine'] == 'toRight' && $next['directionLine'] == 'toBottom' ||
                        $line['directionLine'] == 'toTop' && $next['directionLine'] == 'toRight' ||
                        $line['directionLine'] == 'toLeft' && $next['directionLine'] == 'toTop' ||
                        $line['directionLine'] == 'toBottom' && $next['directionLine'] == 'toLeft'
                    ) {
                        switch (true) {
                            case $line['directionLine'] == 'toRight':
                                $lines[$k]['x2'] = min($line['x2'] + $correct, $params['dx']);
                                $lines[$k + 1]['y1'] = min($next['y1'] + $correct, $params['dy']);
                                break;
                            case $line['directionLine'] == 'toTop':
                                $lines[$k]['y2'] = min($line['y2'] + $correct, $params['dy']);
                                $lines[$k + 1]['x1'] = max($next['x1'] - $correct, 0);
                                break;
                            case $line['directionLine'] == 'toLeft':
                                $lines[$k]['x2'] = max($line['x2'] - $correct, 0);
                                $lines[$k + 1]['y1'] = max($next['y1'] - $correct, 0);
                                break;
                            case $line['directionLine'] == 'toBottom':
                                $lines[$k]['y2'] = max($line['y2'] - $correct, 0);
                                $lines[$k + 1]['x1'] = min($next['x1'] + $correct, $params['dx']);
                                break;
                        }
                    }
                }
                if ($line['cutterLine'] == 'center') {
                    switch (true) {
                        case $line['directionLine'] == 'toRight':
                            $lines[$k]['x2'] = min($line['x2'] + $correct, $params['dx']);
                            $lines[$k + 1]['y1'] = min($next['y1'] + $correct, $params['dy']);
                            break;
                        case $line['directionLine'] == 'toTop':
                            $lines[$k]['y2'] = min($line['y2'] + $correct, $params['dy']);
                            $lines[$k + 1]['x1'] = max($next['x1'] - $correct, 0);
                            break;
                        case $line['directionLine'] == 'toLeft':
                            $lines[$k]['x2'] = max($line['x2'] - $correct, 0);
                            $lines[$k + 1]['y1'] = max($next['y1'] - $correct, 0);
                            break;
                        case $line['directionLine'] == 'toBottom':
                            $lines[$k]['y2'] = max($line['y2'] - $correct, 0);
                            $lines[$k + 1]['x1'] = min($next['x1'] + $correct, $params['dx']);
                            break;
                    }
                }
            }
//            if ($line['cutterLine'] == 'center' && empty($next)) {
//                switch (true) {
//                    case $line['directionY'] == 'horizontal':
////                        $lines[$k]['y1'] += $correct;
////                        $lines[$k]['y2'] += $correct;
//                        break;
//                    case $line['directionX'] == 'toBottom':
////                        $lines[$k]['x1'] += $correct;
////                        $lines[$k]['x2'] += $correct;
//                        break;
//                    case $line['directionLine'] == 'toTop':
////                        $lines[$k]['x1'] -= $correct;
////                        $lines[$k]['x2'] -= $correct;
//                        break;
//                }
//            }

            if ($line['y1'] == $line['y2'] && ($line['y1'] == $params['dy'] || $line['y1'] == 0)) {
                $continue = true;
            }
            if ($line['x1'] == $line['x2'] && ($line['x1'] == $params['dx'] || $line['x1'] == 0)) {
                $continue = true;
            }
            if (!$continue) {
                $result[$group][] = $lines[$k];
            }

            if (!isset($lines[$k + 1])) {
                $group += 1;
            }
        }

        return $result;
    }

    private static function getMatrix($gabarites)
    {
        $ys = array_fill(2, 5, 0);
        $xs = array_fill(4, 11, $ys);

        $ys = array_fill($gabarites['x'], $gabarites[''], 0);
        $xs = array_fill($gabarites[''], $gabarites[''], $ys);
    }

    function getWidthByLines($lines, $cutterLine)
    {
        foreach ($lines as $coordinate) {
            $x[] = $coordinate['x1'];
            $x[] = $coordinate['x2'];
            $y[] = $coordinate['y1'];
            $y[] = $coordinate['y2'];
        }
        return ['x' => min($x), 'y' => min($y), 'w' => max($x) - min($x), 'h' => max($y) - min($y)];
    }

    private static function groupLines(array $programLines, array $params): array
    {
        $figures = [];
        foreach ($programLines as $groupLine) {
            $perimeter = self::getPerimeter($groupLine);
            $figure = array_values(self::mergeLineIntersect($groupLine, $perimeter, $params));
            $explode = self::explodeFigure($figure, $perimeter, $params);

            if ($explode) {
                array_push($figures, ...$explode);
            } else {
                $figures[] = $figure;
            }
        }

        return $figures ?? [];
    }

    public function parseEdgesMaterial(): void
    {
        foreach ($this->xml->good as $goods) {
            if ((string)$goods->attributes()->typeId === 'band') {
                $result = [];
                foreach ($goods->attributes() as $attr) {
                    if (array_key_exists($attr->getName(), self::EXPORT_GOOD)) {
                        $code = $attr->getName();
                        $result[self::EXPORT_GOOD[$code]] = (string)$attr;
                    }
                }
                $this->setEdges($result);
            }
        }
    }

    public function getMaterialIdByOperationId($xml, $operationId, $typeId, $tag, $attr = null): array|string
    {
        $result = $attr ? "" : [];
        foreach ($xml->{$tag} as $operation) {
            $attrOperationId = (int)$operation->attributes()->id;
            if ($attrOperationId === (int)$operationId) {
                $materialId = $operation->children()->attributes()->id ?? false;
                if (!$materialId) {
                    continue;
                }
                foreach ($xml->good as $goods) {
                    $attrTypeId = (string)$goods->attributes()->typeId;
                    if ($attrTypeId === (string)$typeId && ((int)$goods->attributes()->id === (int)$materialId)) {
                        $goodObj = new XmlToJsonConverter($goods);
                        $goodArray = $goodObj->toArray();
                        foreach ($goodArray['good']['_attributes'] as $attrName => $attrValue) {
                            if ($attr) {
                                if ($attrName == $attr) {
                                    return $attrValue;
                                }
                                continue;
                            }
                            if (array_key_exists($attrName, self::EXPORT_GOOD)) {
                                $result[self::EXPORT_GOOD[$attrName]] = $attrValue;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getPartAttributes($attributes, array $attrNames): array
    {
        foreach ($attrNames as $attrName) {
            $result[$attrName] = $attributes[$attrName] ?? false;
        }
        return $result ?? [];
    }

    public function getResult(): array
    {
        if ($this->status === false) {
            return ['message' => $this->message, 'status' => $this->status];
        }
        $result = [];
        $result['department'] = 1;
        $result["glue_type"] = 'EVA';  //Тип клея EVA, PUR
        $result["glue_color"] = 'white';  //transparent - прозрачный, white - белый
        $result['product'] = [];
        $result['materials'] = $this->materials;
        $result['edges'] = $this->edges;
        $result['furnitures'] = [];
        $result['details'] = [];

        foreach ($this->projectArray['project']['good'] as $elements) {
            if ((string)$elements['_attributes']['typeId'] !== 'product' || !isset($elements['part'])) {
                continue;
            }

            $result['product'][] = [
                'id' => (int)$elements['_attributes']['id'],
                'name' => $elements['_attributes']['name']
            ];

            foreach ($elements['part'] as $part) {
                if (isset($part['id'])) {
                    $part['_attributes'] = $part;
                }

                $partId = 0;
                $edgesDetailIds = [];
                $edgesDetailRes = [];
                if (isset($part['_attributes']) || isset($part['id'])) {
                    $partId = (int)$this->getPartAttributes($part['_attributes'], ['id'])['id'];
                }
                if ($partId === 0) {
                    continue;
                }
                $material = $this->getMaterialCSByPartId($partId);
                if (!$material) {
                    continue;
                }
                $edgesOperations = $this->getPartAttributes(
                    $part['_attributes'],
                    array_keys(self::DIRECTOIN)
                );

                $programs = $this->getPartProgramByPartId($partId);
                $grRects = $this->groovingProgram->getRectsByPartId($partId);

                $rectsRes = [];
                $smilesRes = [];
                $cornersRes = [];

                if ($programs) {
                    foreach ($programs as $program) {
                        $params = $this->prepareParams($program['params']);
                        $programLines = $this->getPartProgramLines($program['program'], $params);
                        $corners = $this->corner->getCornerByLines($programLines, $params);
                        $programFigures = self::fixIfOutCutNew($programLines, $params);
                        $programGroupLines = self::groupLines($programFigures, $params);
                        $groovings = $this->grooving->getGroovingByProgram($program['program'], $params);
                        $rects = $this->rect->getRectsByLines($programGroupLines, $params);
                        $buttGroovings = $this->buttGroove->getRectsByLines($programGroupLines, $params);
                        $smiles = $this->smile->getSmileByLines($programGroupLines, $params);

                        array_push($cornersRes, ...$corners);
                        array_push($rectsRes, ...$groovings);
                        array_push($rectsRes, ...$rects);
                        array_push($rectsRes, ...$buttGroovings);
                        array_push($smilesRes, ...$smiles);
                    }
                }

                $holes = $this->bore->getBoredByPartId($partId);
                foreach ($edgesOperations as $direction => $operationIdXML) {
                    $operationId = $operationIdXML ? explode('#', (string)$operationIdXML)[1] : false;
                    $edgesDetailIds[self::DIRECTOIN[$direction]] = $this->getMaterialIdByOperationId(
                        $this->xml,
                        $operationId,
                        'band',
                        'operation',
                        self::SEARCH_ATTR_MATERIAL
                    );
                }
                foreach ($edgesDetailIds as $direct => $detailId) {
                    $edgesDetailRes[$direct] = $this->getIndexEdgesByAttr($detailId);
                }
                if (!isset($material[self::EXPORT_MATERIAL[self::SEARCH_ATTR_MATERIAL]])) {
                    if (preg_match('/\((.*)\)/u', $material['name'], $output_array)) {
                        $m = preg_replace('/[^0-9]/', '', $output_array[0]);
                    }
                } else {
                    $m = $material[self::EXPORT_MATERIAL[self::SEARCH_ATTR_MATERIAL]];
                }
                $detailName = $part['_attributes']['name'] ?? '';
                $result['details'][] = [
                    "productId" => (int)$elements['_attributes']['id'],
                    "name" => $detailName,
                    "detailName" => $detailName,
                    "detailDescription" => $part['_attributes']['description'] ?? '',
                    "material" => $m,
                    "material_id" => $material['id'],
                    "materialCode" => $m,
                    "isRotateTexture" => $part['_attributes']['txt'] == 'true' ?? 'false',
                    "h" => (string)$part['_attributes']['dw'],
                    "l" => (string)$part['_attributes']['dl'],
                    "multiplicity" => 0,
                    "count" => (int)$part['_attributes']['count'],
                    "leftCut" => 0,
                    "rightCut" => 0,
                    "topCut" => 0,
                    "bottomCut" => 0,
                    "holes" => $holes['boreds'],
                    "edges" => $edgesDetailRes,
                    "rects" => array_merge($rectsRes, $grRects),
                    "corners" => $cornersRes,
                    "arcs" => [],
                    "smiles" => $smilesRes,
                ];
            }
        }
        $prepareResult = $this->finalFixed($result);
        return JsonProjectHelper::setHelpersRects($prepareResult);;
    }

    public function setEdges($edges): void
    {
        $this->edges[] = $edges;
    }

    public function setMaterials($sheets): void
    {
        if (!isset($sheets['article'])) {
            if (preg_match('/\((.*)\)/u', $sheets['name'], $output_array)) {
                $sheets['article'] = preg_replace('/[^0-9]/', '', $output_array[0]);
            }
        }
        $this->materials[] = $sheets;
    }

    private function parseSheetsMaterial()
    {
        foreach ($this->xml->good as $goods) {
            if ((string)$goods->attributes()->typeId === 'sheet') {
                $result = [];
                foreach ($goods->attributes() as $attr) {
                    if (array_key_exists($attr->getName(), self::EXPORT_MATERIAL)) {
                        $code = $attr->getName();
                        $result[self::EXPORT_MATERIAL[$code]] = (string)$attr;
                    }
                }
                $this->setMaterials($result);
            }
        }
    }

    private function getMaterialCSByPartId(int $partId): array
    {
        foreach ($this->xml->operation as $operation) {
            if ($operation->attributes()->typeId == 'CS') {
                foreach ($operation->part as $p) {
                    if ($p->attributes()->id == $partId) {
                        foreach ($this->materials as $material) {
                            if ($material['id'] == $operation->material->attributes()->id) {
                                return $material;
                            }
                        }
                    }
                }
            }
        }
        return [];
    }

    private function getIndexEdgesByAttr($edges)
    {
        foreach ($this->edges as $k => $v) {
            if (empty($edges) || empty($v) || !isset($v[self::EXPORT_GOOD[self::SEARCH_ATTR_MATERIAL]])) {
                continue;
            }
            if ($edges == $v[self::EXPORT_GOOD[self::SEARCH_ATTR_MATERIAL]]) {
                return $k;
            }
        }
        return null;
    }

    private function getPartProgramLines(SimpleXMLElement $program, array $params, $groupByMs = false): array
    {
        $result = [];
        $group = 0;
        foreach ($program as $oName => $pr) {
            if ($oName == 'tool') {
                $toolsAttr = (array)$pr->attributes();
                $toolsAttrArr = $toolsAttr['@attributes'];
                $tools[$toolsAttrArr['name']] = $toolsAttrArr;
                $lastToolName = $toolsAttrArr['name'];
                $lastToolD = $toolsAttrArr['d'];
            }
            if (!in_array($oName, ['ms', 'ml', 'mac'])) {
                continue;
            }
            $group++;
            $attr = (array)$pr->attributes();
            $numericAttr[$group] = $attr['@attributes'];

            if ($oName == 'ms') {
                $msAttr = $attr['@attributes'];
                continue;
            }

            $attributes = Coordinate::roundAndConvertLeftBottom($numericAttr[$group], $params);
            $prevAttributes = Coordinate::roundAndConvertLeftBottom($numericAttr[$group - 1], $params);

            switch (true) {
                case $msAttr['c'] == 0:
                    $cutterLine = 'center';
                    break;
                case $msAttr['c'] == 1:
                    $cutterLine = 'left';
                    break;
                case $msAttr['c'] == 2:
                    $cutterLine = 'right';
                    break;
            }

            $directionX = match (true) {
                (float)$prevAttributes['x'] < (float)$attributes['x'] => 'toRight',
                (float)$prevAttributes['x'] > (float)$attributes['x'] => 'toLeft',
                default => 'vertical',
            };
            $directionY = match (true) {
                (float)$prevAttributes['y'] < (float)$attributes['y'] => 'toTop',
                (float)$prevAttributes['y'] > (float)$attributes['y'] => 'toBottom',
                default => 'horizontal',
            };
            $xyDirection = $prevAttributes['y'] == $attributes['y'] ? 'horizontal' : 'vertical';
            switch (true) {
                case $xyDirection == 'horizontal' && ((float)$prevAttributes['x'] < (float)$attributes['x']):
                    $directionLine = 'toRight';
                    break;
                case $xyDirection == 'horizontal' && ((float)$prevAttributes['x'] > (float)$attributes['x']):
                    $directionLine = 'toLeft';
                    break;
                case $xyDirection == 'vertical' && ((float)$prevAttributes['y'] < (float)$attributes['y']):
                    $directionLine = 'toTop';
                    break;
                case $xyDirection == 'vertical' && ((float)$prevAttributes['y'] > (float)$attributes['y']):
                    $directionLine = 'toBottom';
                    break;
                default:
                    $directionLine = 'unknown';
            }

            $toolD = isset($tools[$msAttr['name']]) ? $tools[$msAttr['name']]['d'] : $lastToolD;
            $toolName = isset($tools[$msAttr['name']]) ? $tools[$msAttr['name']]['name'] : $lastToolName;

            $lineResult = [
                'x1' => (float)$prevAttributes['x'],
                'y1' => (float)$prevAttributes['y'],
                'x2' => (float)$attributes['x'],
                'y2' => (float)$attributes['y'],
                'name' => $oName,
                'dp' => $msAttr['dp'] ?? $attributes['dp'],
                'dir' => $attributes['dir'] ?? '',
                'in' => $msAttr['in'] ?? '',
                'cutterLine' => $cutterLine ?? '',
                'cx' => $attributes['cx'] ?? '',
                'cy' => $attributes['cy'] ?? '',
                'directionLine' => $directionLine,
                'directionX' => $directionX,
                'directionY' => $directionY,
                'd' => $toolD,
                'toolName' => $toolName,
                'comment' => $msAttr['comment'] ?? '',
            ];
            $result[$group] = $lineResult;
        }

        return $result;
    }

    public function finalFixed(array $array): array
    {
        foreach ($array['edges'] as $k => &$m) {
            if (!empty($m)) {
                $m['type'] = 'ПВХ';
                $m['laser'] = isset($m['name']) && (bool)preg_match('~\[/ЛАЗ/]~', $m['name']);
            }
        }

        foreach ($array['materials'] as $k => &$m) {
            $m['type'] = match (true) {
                str_starts_with($m['name'], 'ПФ') => 'Постформинг',
                str_starts_with($m['name'], 'HDF') => 'HDF',
                default => 'ЛДСП',
            };

            foreach ($array['details'] as $detail) {
                $matIds[] = $detail['material_id'];
            }
            if (!in_array($m['id'], array_unique($matIds))) {
                unset($array['materials'][$k]);
                continue;
            }

            $materialCodeKey[$m[self::EXPORT_MATERIAL[self::SEARCH_ATTR_MATERIAL]]] = $k;
            unset($m['id']);
        }
        $array['materials'] = array_values($array['materials']);
        foreach ($array['details'] as $k => &$m) {
            $m['material'] = $materialCodeKey[$m['material']] ?? '';
            foreach ($m['corners'] as &$corner) {
                $corner['edge'] = '';
                $sides = explode('_', $corner['angle']);
                foreach ($sides as $side) {
                    if ($m['edges'][$side] !== null) {
                        $corner['edge'] = $m['edges'][$side];
                        break;
                    }
                }

                if ($corner['type'] == 'radius') {
                    $corner['x'] = "";
                    $corner['y'] = "";
                }
            }
            $rectSort = [];
            foreach ($m['rects'] as $kRect => &$rect) {
//                $rect['edgeSide'] = '';
//                $rect['allWidth'] = false;
//                $rect['center'] = false;
//                $rect['offset'] = 0;
                if (!in_array($rect['side'], ['front', 'back'])) {
                    $keyUniq = $rect['side'] . '_' . $rect['width'] . '_' . $rect['height'] . '_' . $rect['depth'];
                    $rectSort[$keyUniq][] = $rect;
                    unset($array['details'][$k]['rects'][$kRect]);
                }
            }
            foreach ($rectSort as &$r) {
                usort($r, function ($a, $b) {
                    return $a['z'] - $b['z'];
                });
            }
            foreach ($rectSort as $v) {
                $first = $v[0];
                $last = end($v);
                if (in_array($first['side'], ['top', 'bottom'])) {
                    $first['height'] = (string)($last['height'] + $last['z'] - $first['z']);
                } else {
                    $first['width'] = (string)($last['width'] + $last['z'] - $first['z']);
                }

                $m['rects'][] = $first;
            }
            $m['rects'] = array_values($m['rects']);
            unset($m['material_id']);
            unset($m['materialCode']);
        }

        return $array;
    }

    private function prepareParams(array $params): array
    {
        $params['mirHor'] = $params['mirHor'] == 'true';
        $params['mirVert'] = $params['mirVert'] == 'true';
        $params['side'] = $params['side'] == 'true';
        return $params;
    }

    private function getPartProgramByPartId(int $partId): array
    {
        $operations = $this->xml->xpath("//operation[part[@id=$partId] and @program]");
        foreach ($operations as $operation) {
            $program = simplexml_load_string($operation->attributes()->program);
            $operationArrtib = (array)$operation->attributes();
            $programAttributes = (array)$program->attributes();
            $attributes = array_intersect_key($operationArrtib['@attributes'], array_flip(self::PARAMS));
            $r[] = ['program' => $program, 'params' => $attributes + $programAttributes['@attributes']];
        }

        return $r ?? [];
    }

    private function cleanEdges()
    {
        $tmp = $edgesArray = [];
        foreach ($this->edges as $m) {
            if (isset($tmp[$m['article']])) {
                continue;
            }
            $tmp[$m['article']] = $m;
            $edgesArray[] = $m;
        }
        $this->edges = $edgesArray;
    }
}
