<?php

namespace Converter;

use Exception;
use Monolog\Level;

class ApiClientKronas
{

    const URL_REGISTER = "register";
    const BASE_URL = "https://auth.kronas.com.ua/api/v1/";
    const URL_LOGIN = "login";
    const URL_LOGOUT = "logout";
    const URL_INFO = "my/info/";
    const URL_USERS = "users";
    const URL_PROJECT_FILE = "project/file";
    const ADD_PROJECT = "project/file";

    public function __construct()
    {
        session_start();
    }

    /**
     * @throws Exception
     */
    private function getResponse(string $action, $data, $method = "POST"): array
    {
        $url = self::BASE_URL . $action;
        $url = $method == 'GET' && is_array($data) ? $url . '?' . http_build_query($data) : $url;
        $curl = curl_init($url);
        $curlParams = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        ];
        if ($method !== strtoupper('GET')) {
            $curlParams[CURLOPT_POSTFIELDS] = $data;
        }
        $curlParams[CURLOPT_HTTPHEADER] = [
            "Accept: application/json",
            "appkey: jkjkhjkhjkeOVCx4HCBe11RvTt0PNqjkhjkhjkhjhkh"
        ];
        if (isset($_SESSION["token"])) {
            $curlParams[CURLOPT_HTTPHEADER][] = "Authorization: Bearer " . $_SESSION["token"];
        }
        curl_setopt_array($curl, $curlParams);
        $response = curl_exec($curl);
        $responseStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        $arrayResponse = json_decode($response, 1);
        $arrayResponse['status'] = true;
        if (isset($_SESSION["token"]) && $responseStatus === 401) {
            unset($_SESSION["token"]);
        }
        if (isset($arrayResponse['token'])) {
            $_SESSION["token"] = $arrayResponse['token'];
        }
        if (!($responseStatus == 200 || $responseStatus == 201)) {
            $arrayResponse['status'] = false;
            (new Log())->add(Level::Error, __CLASS__, $response);
        }

        return $arrayResponse;
    }

    /**
     * @throws Exception
     */
    public function register(array $data = []): array
    {
        return $this->getResponse(self::URL_REGISTER, $data);
    }

    /**
     * @throws Exception
     */
    public function login(array $credentials): array
    {
        return $this->getResponse(self::URL_LOGIN, $credentials);
    }

    /**
     * @throws Exception
     */
    public function logout(): array
    {
        return $this->getResponse(self::URL_LOGOUT, '');
    }

    /**
     * @throws Exception
     */
    public function info(): array
    {
        return $this->getResponse(self::URL_INFO, '', "GET");
    }

    /**
     * @throws Exception
     */
    public function users(): array
    {
        return $this->getResponse(self::URL_USERS, '', "GET");
    }


    /**
     * @throws Exception
     */
    public function addProject(string $jsonProject, $name = null): array
    {

        $q = [
            "name" => date("d-m-y_H-i-s") . ' ' . ($name ?? "name"),
            "file" => $jsonProject,
            "project_folder_id" => null,
            "finalized" => 1
        ];

        return $this->getResponse(self::ADD_PROJECT, $q);
    }

    /**
     * @throws Exception
     */
    public function myProjects(): array
    {
        return $this->getResponse(self::URL_PROJECT_FILE, '', "GET");
    }

    /**
     * @throws Exception
     */
    public function myProjectFile($id): array
    {
        return $this->getResponse(self::URL_PROJECT_FILE . '/' . $id, '', "GET");
    }
}

