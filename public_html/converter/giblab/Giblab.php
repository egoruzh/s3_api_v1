<?php

namespace Converter;

use DOMDocument;
use DOMException;
use Exception;
use json2xml\Project;

class Giblab
{
    private const ACTIONS = [
        'recalculate' => [
            'code' => '4EC657AF',
            'description' => 'Пересчитать проект',
            'params' => ['xncCalcExpr' => 'true'],
            'mustData' => false,
        ],
        'group' => [
            'code' => 'C4F6336B',
            'description' => 'Групповая команда. Выполнение одновременно несколько команд над одним проектом',
            'params' => [],
            'mustData' => true,
        ],
        'optimize_cutting_sheet' => [
            'code' => '55C00560',
            'description' => 'Оптимизация листового раскроя',
            'params' => [],
            'mustData' => false,
        ],
        'running_cutting_optimization' => [
            'code' => '807C17F9',
            'description' => 'Оптимизация погонажного раскроя',
            'params' => [],
            'mustData' => true,
        ],
        'generate_pdf_report' => [
            'code' => '98123591',
            'description' => 'Сгенерировать PDF отчет',
            'params' => [],
            'mustData' => true,
        ],
        'cutting_pdf' => [
            'code' => '7A5D19E5',
            'description' => 'Сгенерировать PDF файл(лы) карт кроя',
            'params' => [],
            'mustData' => true,
        ],
        'xnc_pdf' => [
            'code' => 'D401090B',
            'description' => 'Сгенерировать PDF файл(лы) чертежа деталей и ХNC операций',
            'params' => [],
            'mustData' => true,
        ],
        'lc4' => [
            'code' => 'F6F4A9CC',
            'description' => 'Пильный центр HOLZMA (CadMatic v4.x) lc4',
            'params' => [],
            'mustData' => true,
        ],
        'mpr' => [
            'code' => '298DF080',
            'description' => 'Обрабатывающий центр HOMAG (WoodWop) mpr',
            'params' => [],
            'mustData' => true,
        ],
        'gibcut' => [
            'code' => 'DAD1ABF3',
            'description' => 'Программа для форматно-раскроечного станка GibSaw (gibcut)',
            'params' => [],
            'mustData' => true,
        ],
        'ptx' => [
            'code' => '235F63BD',
            'description' => 'Пильный центр HOLZMA (PTX)',
            'params' => [],
            'mustData' => true,
        ],
    ];

    /**
     * @param string $xml
     * @param string $actionCode
     * @return string
     */

    private static function getResponse(string $xml, string $actionCode): string
    {
        $curl = curl_init();
        //55C00560
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://service.giblab.com/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/xml",
                "action: $actionCode",
                "user-agent: Command"
            ],
        ]);

        $response = curl_exec($curl);
        $error = curl_error($curl);

        curl_close($curl);
        if ($error) {
            return $error;
        }

        return $response;
    }

    /**
     * @throws Exception
     */
    private static function cleanProject(string $project): string
    {
        $projectDom = new DOMDocument();
        libxml_use_internal_errors(true);
        $projectDom->loadXML($project);
        $errors = libxml_get_errors();
        if (!empty($errors)) {
            throw new Exception($errors[0]->message);
        }
        libxml_clear_errors();
        return $projectDom->saveXML($projectDom->documentElement);
    }

    /**
     * @throws DOMException
     * @throws Exception
     */
    public static function prepareRequestXml(string $project, array $params = []): string
    {
        $clearProjectString = self::cleanProject($project);
        $resultDoc = new DOMDocument('1.0', 'utf-8');
        $root = $resultDoc->createElement('request');

        $fragment = $resultDoc->createDocumentFragment();
        $fragment->appendXML($clearProjectString);

        foreach ($params as $name => $value) {
            $paramDom = $resultDoc->createElement('param');
            $paramDom->setAttribute('name', $name);
            $paramDom->setAttribute('value', $value);
            $root->appendChild($paramDom);
        }

        $root->appendChild($fragment);
        $resultDoc->appendChild($root);

        return $resultDoc->saveXML();
    }

    /**
     * @throws DOMException
     * @throws Exception
     */
    public static function getDataProject(string $xml): string|false
    {
        $actionPrepareCode = self::ACTIONS['optimize_cutting_sheet']['code'];
        $requestXml = self::prepareRequestXml($xml);
        $response = self::getResponse($requestXml, $actionPrepareCode);
        if (!preg_match('~[^\x20-\x7E\t\r\n]~', $response)) {
            throw new Exception($response);
        }
        return Helpers::extractZip($response, true);
    }

    /**
     * @throws Exception
     */
    public static function getResultFromArray(string $action, array $projectArray, $returnContentFile = false): bool|array|string
    {
        $projectXml = (new Project($projectArray))->getProject();
        return self::getResultFromXML($action, $projectXml, $returnContentFile);
    }

    /**
     * @throws Exception
     */
    public static function getResultFromXML(string $action, string $projectXml, $returnContentFile = false): bool|array|string
    {
        $actionArray = self::ACTIONS[$action];
        $projectWithData = $actionArray['mustData'] ? self::getDataProject($projectXml) : $projectXml;
        $requestXml = self::prepareRequestXml($projectWithData, $actionArray['params']);
        $response = self::getResponse($requestXml, $actionArray['code']);
        if(!preg_match('~[^\x20-\x7E\t\r\n]~', $response)){
            throw new Exception($response);
        }
        return Helpers::extractZip($response, $returnContentFile);
    }

    /**
     * @throws DOMException
     */
    public static function group(string $xml): bool|array|string
    {
        $xmlWithData = self::getDataProject($xml);
        $actionCode = self::ACTIONS['group']['code'];
        $requestXml = self::prepareRequestXml($xmlWithData, self::ACTIONS['group']['params']);
        $response = self::getResponse($requestXml, $actionCode);
        return Helpers::extractZip($response);
    }

    /**
     * @throws Exception
     */
    public static function getCountMaterialSheet(array $projectArray): array
    {
        $projectXml = (new Project($projectArray))->getProject();

        $result = [];
        $giblabResponse = self::getResultFromXML('optimize_cutting_sheet', $projectXml, true);
        $obj = simplexml_load_string($giblabResponse);
        $operations = $obj->xpath("//operation[@typeId='CS']");
        foreach ($operations as $operation) {
            $articleSheet = (string)$operation->material->attributes()->id;
            $result[$articleSheet]['article'] = $articleSheet;
            foreach ($operation->attributes() as $attrName => $attrValue) {
                if ($attrName == 'data') {
                    continue;
                }
                $result[$articleSheet][$attrName] = (string)$attrValue;
            }
        }
        return $result;
    }
}
