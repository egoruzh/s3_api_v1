<?php
$project_bhx = '';
function build_req_for_project(string $project_bhx, string $string)
{
    return '';
}

$r[] = build_req_for_project($project_bhx, '298DF080');
$r[] = build_req_for_project($project_bhx, '98123591');


$group = [
    '4EC657AF' => [
        'xncCalcExpr'=>true,
    ],
    '55C00560'=>[
        'saveXmlCut'=>true,
    ],
    '7A5D19E5' => [
        'printML' => 15,
        'printMR' => 8,
        'printMT' => 10,
        'printMB' => 10,
    ],
    'D401090B'=>[
        'printML' => 15,
        'printMR' => 8,
        'printMT' => 10,
        'printMB' => 10,
    ],
    '98123591' => [
        'printPH' => false,
        'printM' => false,
        'printRS' => false,
        'printPL' => false,
        'printOL' => false,
        'printXNCStat' => false,
        'printCSh' => false,
        'printCSs' => false,
    ],
    '298DF080'=>[
        'mirVert' => true,
    ],
    '3C1AFC41'=>[
        'mirVert' => true,
    ],

];
$xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<request>
	<!--Recalc-->
	<request action="4EC657AF" save="false">
		<param name="xncCalcExpr" value="true" />	
	</request>
	<!--CS optimization-->
	<request action="55C00560">
		<param name="operationId" value="3" />
		<param name="saveXmlCut" value="true" />
	</request>
	<!--Report patterns-->
	<request action="7A5D19E5" nameTemplate="'patterns/'+project.name+'_'+operation.name">
		<param name="operationId" value="3" />	 
		<!-- nameTemplate="'patterns'" <param name="nameTemplate" value="'patterns'" />-->
		<param name="printML" value="15" />
		<param name="printMR" value="8" />
		<param name="printMT" value="10" />
		<param name="printMB" value="10" />
	</request>
	<!--Report XNC-->
	<request action="D401090B" nameTemplatePart="'xnc/'+part.id+'_'+ns(name)" nameTemplateOperation="'xnc/'+operation.code">
		<param name="printML" value="15" />
		<param name="printMR" value="8" />
		<param name="printMT" value="10" />
		<param name="printMB" value="10" />
	</request>
	<!--Report-->
	<request action="98123591" nameTemplate="'reports/'+project.name">
		<!--<param name="nameTemplate" value="'reports/'+project.name" />-->
		<param name="operationId" value="1" />
		<param name="printPH" value="false" />
		<param name="printM" value="false" />
		<param name="printRS" value="false" />
		<param name="printPL" value="false" />
		<param name="printOL" value="false" />
		<param name="printXNCStat" value="false" />
		<!-- CS -->
		<param name="printCSh" value="false" />
		<param name="printCSs" value="false" />
	</request>

	<!--Postprocessor WoodWop-->
	<request action="298DF080" nameTemplate="'BHX-'+project.name">
		<!--<param name="nameTemplate" value="'BHX-'+project.name" />-->
		<param name="operationId" value="1,2" />
		<param name="prefLongDir" value="1" />
		<param name="mirVert" value="true" />
		<tool name="mill" place="mill" diameter="20" number="101"/>
		<tool name="Bore8" place="front" diameter="8" number="1"/>`
	</request>
	<!--Postprocessor KDT/WDX-->
	<request action="3C1AFC41" nameTemplate="'KDT-'+project.name">
		<!--<param name="nameTemplate" value="'BHX-'+project.name" />-->
		<param name="operationId" value="1,2" />
		<param name="prefLongDir" value="1" />
		<param name="mirVert" value="true" />
		<tool name="mill" place="mill" diameter="20" number="101"/>
		<tool name="Bore8" place="front" diameter="8" number="1"/>`
	</request>
</request>
XML;