<?php

namespace Converter;

class Coordinate
{
    public function isIntersectLines(array $ab, array $cd): bool
    {
        [$inpX1, $inpY1, $inpX2, $inpY2, $inpX3, $inpY3, $inpX4, $inpY4] = [
            $ab['x1'],
            $ab['y1'],
            $ab['x2'],
            $ab['y2'],
            $cd['x1'],
            $cd['y1'],
            $cd['x2'],
            $cd['y2']
        ];
        return self::intersect($inpX1, $inpY1, $inpX2, $inpY2, $inpX3, $inpY3, $inpX4, $inpY4);
    }

    public static function roundAndConvertLeftBottom(array $attributes, array $params): array
    {
        $vars = ['dx' => $params['dx'], 'dy' => $params['dy'], 'dz' => $params['dz']];
        $attributes['x'] = Calculator::getSumByString((string)$attributes['x'], $vars);
        $attributes['y'] = Calculator::getSumByString((string)$attributes['y'], $vars);

        if (isset($attributes['cx'])) {
            $attributes['cx'] = round(Calculator::getSumByString((string)$attributes['cx'], $vars), 1);
        }
        if (isset($attributes['cy'])) {
            $attributes['cy'] = round(Calculator::getSumByString((string)$attributes['cy'], $vars), 1);
        }
        if (isset($attributes['z'])) {
            $attributes['z'] = Calculator::getSumByString((string)$attributes['z'], $vars);
        }

        if ($attributes['x'] < 0) {
            $attributes['x'] = 0;
        }
        if ($attributes['x'] > $params['dx']) {
            $attributes['x'] = $params['dx'];
        }
        if ($attributes['y'] < 0) {
            $attributes['y'] = 0;
        }
        if ($attributes['y'] > $params['dy']) {
            $attributes['y'] = $params['dy'];
        }
        if ($attributes['x'] > $params['dx']) {
            $attributes['x'] = $params['dx'];
        }
        if (!$params['mirVert']) {
            $attributes['y'] = abs($attributes['y'] - $params['dy']);
        }
        if ($params['mirHor']) {
            $attributes['x'] = abs($attributes['x'] - $params['dx']);
        }
        return $attributes;
    }

    /**
     * Пересечение прямоугольников прямых (проекций на оси xy)
     * @param $x1
     * @param $y1
     * @param $x2
     * @param $y2
     * @param $x3
     * @param $y3
     * @param $x4
     * @param $y4
     * @return bool
     */
    private static function rectanglesIntersects($x1, $y1, $x2, $y2, $x3, $y3, $x4, $y4): bool
    {
        if (gmp_sign($x3 - $x2) * gmp_sign($x4 - $x1) > 0) {
            return false;
        }

        if (gmp_sign($y3 - $y2) * gmp_sign($y4 - $y1) > 0) {
            return false;
        }

        return true;
    }

    public static function intersect($x1, $y1, $x2, $y2, $x3, $y3, $x4, $y4): bool
    {
        $arrXY = [&$x1, &$y1, &$x2, &$y2, &$x3, &$y3, &$x4, &$y4];
        foreach ($arrXY as &$xy) {
            $xy *= 1000;
        }
        $minX1X2 = min($x1, $x2);
        $minY1Y2 = min($y1, $y2);
        $maxX1X2 = max($x1, $x2);
        $maxY1Y2 = max($y1, $y2);
        $minX3X4 = min($x3, $x4);
        $minY3Y4 = min($y3, $y4);
        $maxX3X4 = max($x3, $x4);
        $maxY3Y4 = max($y3, $y4);

        if (!self::rectanglesIntersects(
            $minX1X2,
            $minY1Y2,
            $maxX1X2,
            $maxY1Y2,
            $minX3X4,
            $minY3Y4,
            $maxX3X4,
            $maxY3Y4
        )) {
            return false;
        }

        $ACx = $x3 - $x1;
        $ACy = $y3 - $y1;

        $ABx = $x2 - $x1;
        $ABy = $y2 - $y1;

        $ADx = $x4 - $x1;
        $ADy = $y4 - $y1;

        $CAx = $x1 - $x3;
        $CAy = $y1 - $y3;

        $CBx = $x2 - $x3;
        $CBy = $y2 - $y3;

        $CDx = $x4 - $x3;
        $CDy = $y4 - $y3;

        $ACxAB = ($ACx * $ABy) - ($ACy * $ABx);

        $ADxAB = ($ADx * $ABy) - ($ADy * $ABx);

        $CAxCD = ($CAx * $CDy) - ($CAy * $CDx);

        $CBxCD = ($CBx * $CDy) - ($CBy * $CDx);

        if ((gmp_sign($ACxAB) * gmp_sign($ADxAB) > 0) || (gmp_sign($CAxCD) * gmp_sign($CBxCD) > 0)) {
            return false;
        }

        return true;
    }

    /**
     * Принадлежность точки прямоугольнику
     * @param $px //координаты точки
     * @param $py //координаты точки
     * @param $x //минимальная точка прямоугольника по x
     * @param $y //минимальная точка прямоугольника по y
     * @param $h //высота прямоугольника (y)
     * @param $w //ширина прямоугольника (x)
     * @return bool
     */
    public static function inside($px, $py, $x, $y, $h, $w): bool
    {
        return ($px >= $x && $px <= $x + $w && $py >= $y && $py <= $y + $h);
    }

    private static function insideIn($px, $py, $x, $y, $h, $w): bool
    {
        return ($px > $x && $px < $x + $w && $py > $y && $py < $y + $h);
    }

    public static function lineIn($px1, $py1, $px2, $py2, $x, $y, $h, $w): bool
    {
        return self::inside($px1, $py1, $x, $y, $h, $w) && self::insideIn($px2, $py2, $x, $y, $h, $w) || self::inside(
                $px2,
                $py2,
                $x,
                $y,
                $h,
                $w
            ) && self::insideIn($px1, $py1, $x, $y, $h, $w);
    }
}