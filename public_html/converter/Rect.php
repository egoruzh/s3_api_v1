<?php

namespace Converter;

class Rect
{
    private static function validateFigure(array $groupLine, array $params): bool
    {
        $groupLine = array_values($groupLine);
//        d(count($groupLine) == 1 , $groupLine[0]['in'] == 2, $groupLine);
        if (count($groupLine) == 1 && $groupLine[0]['in'] == 2) {
            return false;
        }
        $linaNames = array_column($groupLine, 'name');
        $countLines = array_count_values($linaNames);
        $cMac = $countLines['mac'] ?? 0;
        $cMl = $countLines['ml'] ?? 0;
        if ($cMac && $cMac > 1 && $cMl && $cMl < 2) {
            if ($cMac > $cMl) {
                return false;
            }
        }
        if (in_array('mac', $linaNames) && !in_array('ml', $linaNames)) {
            return false;
        }
        if (count($groupLine) == 2) {
            foreach ($linaNames as $name) {
                if ($name !== 'ml') {
                    return false;
                }
                if ($groupLine[0]['x1'] - $groupLine[0]['x2'] != 0 && $groupLine[0]['y1'] - $groupLine[0]['y2'] != 0) {
                    return false;
                }
                if ($groupLine[0]['x1'] - $groupLine[0]['x2'] != 0) {
                    $k1 = ($groupLine[0]['y2'] - $groupLine[0]['y1']) / ($groupLine[0]['x2'] - $groupLine[0]['x1']);
                } else {
                    $k1 = 0;
                }
                if ($groupLine[1]['x1'] - $groupLine[1]['x2'] != 0) {
                    $k2 = ($groupLine[1]['y2'] - $groupLine[1]['y1']) / ($groupLine[1]['x2'] - $groupLine[1]['x1']);
                } else {
                    $k2 = 0;
                }
                if (!($k1 == 0 && $k1 == $k2)) {
                    return false;
                }
            }
        }
        if (count($groupLine) == 3) {
            if (!in_array('mac', $linaNames)) {
                if (($groupLine[1]['x1'] != $groupLine[1]['x2']) && ($groupLine[1]['y1'] != $groupLine[1]['y2'])) {
                    return false;
                }
                if ($groupLine[0]['x1'] == 0 && $groupLine[2]['x2'] != 0) {
                    return false;
                }

                switch (true) {
                    case ($groupLine[1]['x1'] != $groupLine[1]['x2']) && ($groupLine[1]['y1'] != $groupLine[1]['y2']):
                    case $groupLine[0]['x1'] == 0 && $groupLine[2]['x2'] != 0:
                    case $groupLine[0]['y1'] == 0 && $groupLine[2]['y2'] != 0:
                    case $groupLine[0]['x1'] == $params['dx'] && $groupLine[2]['x2'] != $params['dx']:
                    case $groupLine[0]['y1'] == $params['dy'] && $groupLine[2]['y2'] != $params['dy']:
                        return false;
                }
            }
            if (in_array('mac', $linaNames)) {
                if ((($groupLine[0]['x1'] == $params['dx'] || $groupLine[0]['x1'] == 0) && ($groupLine[2]['y2'] == $params['dy'] || $groupLine[2]['y2'] == 0)) ||

                    (($groupLine[0]['y1'] == $params['dy'] || $groupLine[0]['y1'] == 0) && ($groupLine[2]['x2'] == $params['dx'] || $groupLine[2]['x2'] == 0))) {
//на углу
                    $k1 = ($groupLine[1]['y2'] - $groupLine[1]['y1']) / ($groupLine[1]['x2'] - $groupLine[1]['x1']);
                    if (abs($k1) != 1) {
                        return false;
                    }
                } elseif ($groupLine[1]['y1'] != $groupLine[1]['y2'] && $groupLine[1]['x1'] != $groupLine[1]['x2']) {
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function getRectsByLines(array $groupLines, array $params): array
    {
        foreach ($groupLines as $groupLine) {
            $v = self::validateFigure($groupLine, $params);
            $rect = self::getRectFormated($groupLine, $params);
            if (empty($groupLine) || !$v || empty($rect)) {
                continue;
            }
            $result[] = $rect;
        }

        return $result ?? [];
    }

    private function getRectFormated(array $coordinate, $params): array
    {
        $macArr = [];
        $x = $y = [];
        $coordinate = array_values($coordinate);

        foreach ($coordinate as $c) {
            $x1 = $c['x1'];
            $x2 = $c['x2'];
            $y1 = $c['y1'];
            $y2 = $c['y2'];
            $params['turn'] = $params['turn'] ?? 0;
            switch (true) {
                case $params['turn'] == 1:
                    $c['y1'] = $x1;
                    $c['y2'] = $x2;
                    $c['x1'] = $params['dy'] - $y1;
                    $c['x2'] = $params['dy'] - $y2;
                    $newParams['dx'] = $params['dy'];
                    $newParams['dy'] = $params['dx'];
                    break;
                case $params['turn'] == 2:
                    $c['x1'] = $params['dx'] - $x1;
                    $c['x2'] = $params['dx'] - $x2;
                    $c['y1'] = $params['dy'] - $y1;
                    $c['y2'] = $params['dy'] - $y2;
                    $newParams['dx'] = $params['dx'];
                    $newParams['dy'] = $params['dy'];
                    break;
                case $params['turn'] == 3:
                    $c['x1'] = $y1;
                    $c['x2'] = $y2;
                    $c['y1'] = $params['dx'] - $x1;
                    $c['y2'] = $params['dx'] - $x2;
                    $newParams['dx'] = $params['dy'];
                    $newParams['dy'] = $params['dx'];
                    break;
                default:
                    $newParams['dx'] = $params['dx'];
                    $newParams['dy'] = $params['dy'];
            }

            $x[] = $c['x1'];
            $x[] = $c['x2'];
            $y[] = $c['y1'];
            $y[] = $c['y2'];
            if ($c['name'] == 'mac') {
                $macArr[] = $c;
            }
        }

        $mac = match (true) {
            isset($macArr[1]) => $macArr[1],
            isset($macArr[0]) => $macArr[0],
            default => [],
        };

        $minX = min($x);
        $minY = min($y);
        $maxX = max($x);
        $maxY = max($y);

        if (count($coordinate) == 1) {
            if ($coordinate[0]['directionLine'] == 'toBottom') {
                if ($coordinate[0]['cutterLine'] == 'right') {
                    $minX -= (float)$coordinate[0]['d'];
                }
            }
            if ($coordinate[0]['directionLine'] == 'toTop') {
                if ($coordinate[0]['cutterLine'] == 'right') {
                    $maxX += (float)$coordinate[0]['d'];
                }
            }
            if ($coordinate[0]['directionLine'] == 'toLeft') {
                if ($coordinate[0]['cutterLine'] == 'right') {
                    $maxY += (float)$coordinate[0]['d'];
                }
            }
            if ($coordinate[0]['directionLine'] == 'toRight') {
                if ($coordinate[0]['cutterLine'] == 'right') {
                    $minY -= (float)$coordinate[0]['d'];
                }
            }

            if ($coordinate[0]['directionLine'] == 'toBottom' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'left') {
                    $maxX += (float)$coordinate[0]['d'];
                }
            }
            if ($coordinate[0]['directionLine'] == 'toTop' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'left') {
                    $minX -= (float)$coordinate[0]['d'];
                }
            }
            if ($coordinate[0]['directionLine'] == 'toLeft' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'left') {
                    $minY -= (float)$coordinate[0]['d'];
                }
            }

            if ($coordinate[0]['directionLine'] == 'toRight' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'left') {
                    $maxY += (float)$coordinate[0]['d'];
                }
            }

            if ($coordinate[0]['directionLine'] == 'toBottom' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'center') {
                    $maxX += (float)$coordinate[0]['d'] / 2;
                    $minX -= (float)$coordinate[0]['d'] / 2;
                }
            }
            if ($coordinate[0]['directionLine'] == 'toTop' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'center') {
                    $minX -= (float)$coordinate[0]['d'] / 2;
                    $maxX += (float)$coordinate[0]['d'] / 2;
                }
            }
            if ($coordinate[0]['directionLine'] == 'toLeft' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'center') {
                    $minY -= (float)$coordinate[0]['d'] / 2;
                    $maxY += (float)$coordinate[0]['d'] / 2;
                }
            }

            if ($coordinate[0]['directionLine'] == 'toRight' && $coordinate[0]['dp'] < $params['dz']) {
                if ($coordinate[0]['cutterLine'] == 'center') {
                    $maxY += (float)$coordinate[0]['d'] / 2;
                    $minY -= (float)$coordinate[0]['d'] / 2;
                }
            }
        }

        if ($minX < 0) {
            $minX = 0;
        }
        if ($maxX > $newParams['dx']) {
            $maxX = $newParams['dx'];
        }

        if ($minY < 0) {
            $minY = 0;
        }
        if ($maxY > $newParams['dy']) {
            $maxY = $newParams['dy'];
        }

        $width = (string)abs(($maxX - $minX));
        $height = (string)abs(($maxY - $minY));

        if (count($coordinate) == 1) {
            if ($coordinate[0]['directionY'] == 'horizontal') {
                if ($width < $coordinate[array_key_first($coordinate)]['d']) {
                    return [];
                }
            }
            if ($coordinate[0]['directionX'] == 'vertical') {
                if ($height < $coordinate[array_key_first($coordinate)]['d']) {
                    return [];
                }
            }
        }
        $firstCoordinate = $coordinate[array_key_first($coordinate)];
        $result['side'] = $params['side'] ? 'front' : 'back';
        $result['x'] = (string)$minX;
        $result['y'] = (string)$minY;
        $result['z'] = 0;
        $result['width'] = $width;
        $result['height'] = $height;
        $result['depth'] = (string)$firstCoordinate['dp'];
        $result['r'] = $mac ? abs($mac['x2'] - $mac['x1']) : 0;
        $result['edge'] = null;
        $result['ext'] = false;
        $result['comment'] = $firstCoordinate['comment'] ?? '';

        return $result;
    }
}
