<?php

namespace Converter;

class ButtGroove
{
    private static function validateFigure(array $groupLine, array $params): bool
    {
        if (count($groupLine) != 1) {
            return false;
        }
        $groupLine = array_values($groupLine)[0];
        if ($groupLine['name'] != 'ml' || $groupLine['in'] != 2) {
            return false;
        }

        return true;
    }

    public function getRectsByLines(array $programGroupLines, array $params): array
    {
        foreach ($programGroupLines as $groupLine) {
            $v = self::validateFigure($groupLine, $params);
            if (!$v) {
                continue;
            }
            $result[] = self::getRectFormated($groupLine, $params);
        }

        return $result ?? [];
    }

    private function getRectFormated(array $coordinate, $params): array
    {
        $middleX = $params['dx'] / 2;
        $middleY = $params['dy'] / 2;
        $x = $y = $dp = [];
        foreach ($coordinate as $c) {
            $x[] = $c['x1'];
            $x[] = $c['x2'];
            $y[] = $c['y1'];
            $y[] = $c['y2'];
            $dp[] = $c['dp'];
        }
        $dpMax = max($dp);
        $dpMin = min($dp);

        if (count(array_unique($y)) == 1) {
            $side = $y[0] < $middleY ? 'bottom' : 'top';
        } else {
            $side = $x[0] < $middleX ? 'left' : 'right';
        }

        $minX = min($x);
        $maxX = max($x);
        $minY = min($y);
        $maxY = max($y);
        $wPaz = $dpMax - $dpMin + 2.5;
        $width = ($side == 'bottom' || $side == 'top') ? abs($maxX - $minX) : $wPaz;
        $z = $params['side'] ? $params['dz'] - $dpMax : $dpMax;
        $height = ($side == 'bottom' || $side == 'top') ? $wPaz : abs($maxY - $minY);

        $depth = match (true) {
            $side == 'bottom' => $y[0],
            $side == 'top' => $params['dy'] - $y[0],
            $side == 'left' => $x[0],
            $side == 'right' => $params['dx'] - $x[0],
        };
        $ext = match (true) {
            $side == 'bottom' && ($params['dx'] - $maxX) == 0, $side == 'top' && ($params['dx'] - $maxX) == 0, $side == 'left' && ($params['dy'] - $maxY) == 0, $side == 'right' && ($params['dy'] - $maxY) == 0 => true,
            default => false,
        };

        $result['side'] = $side;
        $result['x'] = $ext ? '0' : $minX;
        $result['y'] = $ext ? '0' : $minY;
        $result['z'] = (string)$z;
        $result['width'] = (string)$width;
        $result['height'] = (string)$height;
        $result['depth'] = (string)$depth;
        $result['r'] = 0;
        $result['edge'] = null;
        $result['ext'] = false;
        $result['comment'] = $coordinate['comment'] ?? '';

        return $result;
    }
}