<?php

namespace json2xml;

use Exception;
use SimpleXMLElement;

class Project
{
    public SimpleXMLElement $xml;
    public array $projectArray;
    public array $usedMaterial = [];
    public const EDGE_ATTRIBUTES = [
        'top' => 'elt',
        'right' => 'elr',
        'bottom' => 'elb',
        'left' => 'ell',
    ];
    const  MATERIAL_TEMPLATE = [
        'width' => '1830',
        'height' => '2750',
        'thickness' => '18',
        'article' => '4000',
        'name' => 'Каркас',
        'id' => '3000',
        'material_id' => '194',
        'type' => 'ЛДСП'
    ];
    public const PROJECT_FIELDS = [
        'edges' => 'array',
        'details' => 'array',
        'product' => 'array',
        'glue_type' => 'string',
        'materials' => 'array',
        'furnitures' => 'array',
        'glue_color' => 'string'
    ];
    public const PROJECT_PRODUCT_FIELDS = [
        'name'
    ];

    public const PROJECT_MATERIAL_FIELDS = [
        'article',
        'height',
        'name',
        'thickness',
        'width',
        'type'
    ];
    public const PROJECT_EDGE_FIELDS = [
        'article',
        'name',
        'thickness',
        'width',
        'type',
        'laser'
    ];
    public const DETAIL_FIELDS = [
        'productId',
        'material',
        'isRotateTexture',
        'h',
        'l',
        'multiplicity',
        'count',
        'holes',
        'edges',
        'rects',
        'corners',
        'arcs',
        'smiles',
    ];

    public const DETAIL_HOLE_FIELDS = ['x', 'y', 'diam', 'side', 'depth'];
    public const DETAIL_EDGE_FIELDS = ['left', 'right', 'top', 'bottom'];
    public const DETAIL_RECT_FIELDS = ['x', 'y', 'width', 'height', 'depth', 'r', 'side'];
    public const DETAIL_CORNER_FIELDS = ['x', 'y', 'r', 'type', 'angle'];
    private array $productParts = [];

    /**
     * @throws Exception
     */
    public function __construct(array $projectArray)
    {
        $this->projectArray = $projectArray;
        $this->checkStructure()->prepareJson();
        $time = time() * 1000;
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<project costOperation="0" costMaterial="0" description="converted from json" cost="0" name="New" currency="грн" date="$time" orderDate="$time" version="21052101"/>
XML;
        $this->xml = simplexml_load_string($xml);
    }

    /**
     * @throws Exception
     */
    private function prepareJson(): void
    {
        $this->projectArray['materials'][] = self::MATERIAL_TEMPLATE;
        $partId = 2000;
        foreach ($this->projectArray['product'] as $p) {
            $this->productParts[$p['id']] = $p;
        }
        foreach ($this->projectArray['details'] as &$detail) {
            $partId++;
            $detail['partId'] = $partId;
            $key = $detail['material'] === null ? array_key_last($this->projectArray['materials']) : $detail['material'];
            $materialParts[$key][] = $partId;
            $this->usedMaterial[$key] = $this->projectArray['materials'][$key];
            $detail['material'] = $this->projectArray['materials'][$key];
            $this->usedMaterial[$key]['parts'] = $materialParts[$key];
            $this->productParts[$detail['productId']]['details'][] = $detail;
        }
    }

    /**
     * @throws Exception
     */
    public function getProject(): bool|string
    {
        $eggeResultArray = [];
        $operationId = 2000;

        foreach ($this->productParts as $productId => $product) {
            $goodProduct = $this->xml->addChild('good');
            $goodProduct->addAttribute('typeId', 'product');
            $goodProduct->addAttribute('id', $productId);
            $goodProduct->addAttribute('name', $product['name']);
            foreach ($product['details'] ?? [] as $detail) {
                $detailName = $detail['detailName'] ?? $detail['name'] ?? '';
                $partProduct = $goodProduct->addChild('part');
                $partProduct->addAttribute('count', $detail['count']);
                $partProduct->addAttribute('name', $detailName);
                $partProduct->addAttribute('description', $detail['detailDescription'] ?? '');
                $partProduct->addAttribute('l', $detail['l']);
                $partProduct->addAttribute('w', $detail['h']);
                $partProduct->addAttribute('dl', $detail['l']);
                $partProduct->addAttribute('dw', $detail['h']);
                $partProduct->addAttribute('jl', $detail['l']);
                $partProduct->addAttribute('jw', $detail['h']);
                $partProduct->addAttribute('cl', $detail['l']);
                $partProduct->addAttribute('cw', $detail['h']);
                $partProduct->addAttribute('txt', $detail['isRotateTexture'] ? 'true' : 'false');
                $partProduct->addAttribute('id', $detail['partId']);
                $partProduct->addAttribute('my_material_code', $detail['material']['article']);
                foreach ($detail['edges'] as $direction => $edgeKey) {
                    if ($edgeKey !== null) {
//                        dd($this->json['edges'], $edgeKey);
                        $partProduct->addAttribute(
                            self::EDGE_ATTRIBUTES[$direction],
                            '@operation#' . $this->projectArray['edges'][$edgeKey]['article']
                        );
                        $eggeResultArray[$this->projectArray['edges'][$edgeKey]['article']][$detail['partId']] = $this->projectArray['edges'][$edgeKey];
                    }
                }
                $programs = $this->gerDetailProgramsXNC($detail);
                foreach ($programs as $program) {
                    $operationId++;
                    $operation = $this->xml->addChild('operation');
                    $operation->addAttribute('typeId', 'XNC');
                    $side = $program['attributes']['side'] ? 'true' : 'false';
                    $operation->addAttribute('side', $side);
                    $operation->addAttribute('program', $program['program']->saveXML());
                    $operation->addAttribute('bySizeDetail', 'true');
                    $operation->addAttribute('mirHor', 'false');
                    $operation->addAttribute('mirVert', 'true');
                    $operation->addAttribute('count', $detail['count']);
                    $operation->addAttribute('countBore', count($detail['holes']));
                    $operation->addAttribute('countMill', '0');
                    $operation->addAttribute('countCut', '0');
                    $operation->addAttribute('id', $operationId);
                    $operation->addAttribute('name', 'Деталь id=' . $detail['partId']);
                    $operation->addAttribute('typeName', 'Деталь id=' . $detail['partId']);
                    $operation->addChild('part')->addAttribute('id', $detail['partId']);
                }
            }
        }

        foreach ($this->usedMaterial as $material) {
            $cTrimL = $material['type'] == 'Постформинг' ? -1 : 8;
            $sheetPartId = $material['article'] * 10;
            $goodSheet = $this->xml->addChild('good');
            $goodSheet->addAttribute('typeId', 'sheet');
            $goodSheet->addAttribute('id', $material['article']);
            $goodSheet->addAttribute('name', $material['name']);
            $goodSheet->addAttribute('t', $material['thickness']);
            $goodSheet->addAttribute('unit', 'м.кв.');
            $goodSheet->addAttribute('code', $material['article']);
            $goodSheet->addAttribute('l', $material['height']);
            $goodSheet->addAttribute('w', $material['width']);
            $sheetPart = $goodSheet->addChild('part');
            $sheetPart->addAttribute('id', $sheetPartId);
            $sheetPart->addAttribute('usedCount', 0);
            $sheetPart->addAttribute('count', 10000);
            $sheetPart->addAttribute('l', $material['height']);
            $sheetPart->addAttribute('w', $material['width']);

            $operationId++;
            $cs = $this->xml->addChild('operation');
            $cs->addAttribute('typeId', 'CS');
            $cs->addAttribute('csTexture', 'true');
            $cs->addAttribute('cSizeMode', '1');
            $cs->addAttribute('l', $material['height']);
            $cs->addAttribute('w', $material['width']);
            $cs->addAttribute('t', $material['thickness']);
            $cs->addAttribute('cTrimL', $cTrimL);
            $cs->addAttribute('cTrimW', 10);
            $cs->addAttribute('cTrimWaste', 'false');
            $cs->addAttribute('tool1', '11111');
            $cs->addAttribute('id', $operationId);
            $cs->addChild('material')->addAttribute("id", $material['article']);
            $cs->addChild('part')->addAttribute('id', $sheetPartId);
            foreach ($material['parts'] as $part) {
                $cs->addChild('part')->addAttribute('id', $part);
            }
        }
        foreach ($this->projectArray['edges'] as $edge) {
            if (!isset($edge['article'])) {
                continue;
            }
            $goodSheet = $this->xml->addChild('good');
            $goodSheet->addAttribute('typeId', 'band');
            $goodSheet->addAttribute('id', $edge['article']);
            $goodSheet->addAttribute('name', $edge['name']);
            $goodSheet->addAttribute('t', $edge['thickness']);
            $goodSheet->addAttribute('unit', 'м.п.');
            $goodSheet->addAttribute('code', $edge['article']);
            $goodSheet->addAttribute('w', $edge['width']);
        }

        $this->setEdgeOperation($eggeResultArray);
        $toolCutting = $this->xml->addChild('good');
        $toolCutting->addAttribute('typeId', 'tool.cutting');
        $toolCutting->addAttribute('id', '11111');

        return $this->xml->asXML();
    }

    private function gerDetailProgramsXNC(array $detail): array
    {
        $detail['dz'] = (float)$detail['material']['thickness'];
        $program = $result = [];
        $countBore = 0;
        foreach ($detail['holes'] as $hole) {
            $countBore++;
            if ($hole['side'] == 'back') {
                $program['back']['holes'][] = $hole;
            } else {
                $program['front']['holes'][] = $hole;
            }
        }
        foreach ($detail['rects'] as $rect) {
            if ($rect['side'] == 'back') {
                $program['back']['rects'][] = $rect;
            } else {
                $program['front']['rects'][] = $rect;
            }
        }

        if (!empty($detail['corners'])) {
            $program['front']['corners'] = $detail['corners'];
        }
        foreach ($program as $side => $operations) {
            $attr['side'] = $side == 'front';
            $attr['countBore'] = $countBore;
            $result[] = [
                'program' => (new Program($operations, $detail, $side == 'front'))->getProgram(),
                'attributes' => $attr
            ];
        }
        return $result;
    }

    private function setEdgeOperation(array $eggeResultArray): void
    {
        foreach ($eggeResultArray as $operationId => $edges) {
            $goodSheet = $this->xml->addChild('operation');
            $goodSheet->addAttribute('tool1', $operationId * 100);
            $goodSheet->addAttribute('typeId', 'EL');
            $goodSheet->addAttribute('id', $operationId);
            $goodSheet->addChild('material')->addAttribute('id', $operationId);
            $toolCutting = $this->xml->addChild('good');
            $toolCutting->addAttribute('typeId', 'tool.edgeline');
            $toolCutting->addAttribute('id', $operationId * 100);

            foreach (array_keys($edges) as $partId) {
                $goodSheet->addChild('part')->addAttribute('id', $partId);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function checkStructure(): self
    {
        $errors = [];
        $errorProject = array_diff(array_keys(self::PROJECT_FIELDS), array_keys($this->projectArray));

        if (!empty($errorProject)) {
            $errors['required'] = array_values($errorProject);
        }

        if (empty($errors)) {
            foreach (self::PROJECT_FIELDS as $fieldName => $type){
                if (gettype($this->projectArray[$fieldName]) !== $type)
                {
                    $errors['types']['project'][$fieldName] = 'must be '. $type;
                }
            }
        }
        if (empty($errors)) {
            foreach ($this->projectArray['details'] as $key => $detail) {
                if(!is_array($detail)){
                    $errors['types']['root_detail'] = 'must contain an array of objects';
                    continue;
                }
                $numKey = $key + 1;
                $check = array_diff(self::DETAIL_FIELDS, array_keys($detail));
                if (!empty($check)) {
                    $errors['required']['detail#' . $numKey] = array_values($check);
                }
            }
            foreach ($this->projectArray['edges'] as $key => $edge) {
                if(!is_array($edge)){
                    $errors['types']['root_edge'] = 'must contain an array of objects';
                    continue;
                }
                $check = array_diff(self::PROJECT_EDGE_FIELDS, array_keys($edge));
                if (!empty($check)) {
                    $errors['required']['edge#' . $key + 1] = array_values($check);
                }
            }
            foreach ($this->projectArray['materials'] as $key => $material) {
                if(!is_array($material)){
                    $errors['types']['root_material'] = 'must contain an array of objects';
                    continue;
                }
                $check = array_diff(self::PROJECT_MATERIAL_FIELDS, array_keys($material));
                if (!empty($check)) {
                    $errors['required']['material#' . $key + 1] = array_values($check);
                }
            }
            foreach ($this->projectArray['product'] as $key => $product) {
                if(!is_array($product)){
                    $errors['types']['root_product'] = 'must contain an array of objects';
                    continue;
                }
                $check = array_diff(self::PROJECT_PRODUCT_FIELDS, array_keys($product));
                if (!empty($check)) {
                    $errors['required']['product#' . $key + 1] = array_values($check);
                }
            }
        }

        if (empty($errors)) {

            foreach ($this->projectArray['details'] as $keyDetail => $detail) {
                $detailKeyText = 'detail#' . $keyDetail + 1;

                if(!is_array($detail['edges'])){
                    $errors['types'][$detailKeyText]['edges'] = 'must be array';
                    continue;
                }
                $checkDetailEdge = array_diff(self::DETAIL_EDGE_FIELDS, array_keys($detail['edges']));
                if (!empty($checkDetailEdge)) {
                    $errors['required'][$detailKeyText]["edge"] = array_values($checkDetailEdge);
                }

                if(!is_array($detail['holes'])){
                    $errors['types'][$detailKeyText]['holes'] = 'must array';
                    continue;
                }
                foreach ($detail['holes'] as $key => $hole) {
                    if(!is_array($hole)){
                        $errors['types'][$detailKeyText]['hole'] = 'must be array';
                        continue;
                    }
                    $check = array_diff(self::DETAIL_HOLE_FIELDS, array_keys($hole));
                    if (!empty($check)) {
                        $errors['required'][$detailKeyText]['hole#' . $key + 1] = array_values($check);
                    }
                }

                if(!is_array($detail['corners'])){
                    $errors['types'][$detailKeyText]['corners'] = 'must array';
                    continue;
                }
                foreach ($detail['corners'] as $key => $corner) {
                    if(!is_array($corner)){
                        $errors['types'][$detailKeyText]['corner'] = 'must be array';
                        continue;
                    }
                    $check = array_diff(self::DETAIL_CORNER_FIELDS, array_keys($corner));
                    if (!empty($check)) {
                        $errors['required'][$detailKeyText]['corner#' . $key + 1] = array_values($check);
                    }
                }

                if(!is_array($detail['rects'])){
                    $errors['types'][$detailKeyText]['rects'] = 'must array';
                    continue;
                }
                foreach ($detail['rects'] as $key => $rect) {
                    if(!is_array($rect)){
                        $errors['types'][$detailKeyText]['rect'] = 'must be array';
                        continue;
                    }
                    $check = array_diff(self::DETAIL_RECT_FIELDS, array_keys($rect));
                    if (!empty($check)) {
                        $errors['required'][$detailKeyText]['rect#' . $key + 1] = array_values($check);
                    }
                }
            }
        }
        if (!empty($errors)) {
            throw new Exception(json_encode($errors));
        }
        return $this;
    }
}