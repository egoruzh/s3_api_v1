<?php

namespace Converter;

use json2xml\Program;

class JsonProjectHelper
{
    public const RECT_TYPES = [
        'P_SHAPE' => 'UShape',
        'RECTANGLE' => 'Rectangle',
        'RABBET' => 'Rabbet',
        'GROOVE' => 'Groove',
    ];

    public const SIDE_BUTT = [
        'bottom',
        'top',
        'left',
        'right',
    ];
    public static function cleanEdge(array &$project): array
    {
        $indexes = [];
        foreach ($project['details'] as $detail) {
            foreach ($detail['edges'] as $edge) {
                if ($edge !== null) {
                    $indexes[(int)$edge] = (int)$edge;
                }
            }
        }

        $edges = array_intersect_key($project['edges'], $indexes);
        $project['edges'] = $edges;
        return $project;
    }

    public static function cleanMaterial(array &$project): array
    {
        $indexes = [];
        foreach ($project['details'] as $detail) {
            if ($detail['material'] !== null) {
                $indexes[(int)$detail['material']] = (int)$detail['material'];
            }
        }
        $materials = array_intersect_key($project['materials'], $indexes);
        $project['materials'] = $materials;
        return $project;
    }

    public static function setHelpersRects(array &$project): array
    {
        foreach ($project['materials'] as &$material) {
            foreach ($material as &$value){
                if(is_numeric($value)){
                    $value = (float)$value;
                }
            }
        }
        foreach ($project['details'] as &$detail) {
            $thickness = $project['materials'][$detail['material']]['thickness'];
            $detail['l'] = (float)$detail['l'];
            $detail['h'] = (float)$detail['h'];
            $detail['productId'] = (int)$detail['productId'];
            foreach($detail['holes'] as  &$hole){
                $through = $hole['depth'] >= $thickness;
                $hole['depth'] = in_array($hole['side'], self::SIDE_BUTT) ?
                    $hole['depth'] :
                    ($through ? $thickness + Program::BORE_THROUGH_DZ_PLUS : $hole['depth']);
                foreach ($hole as &$op){
                    if(is_numeric($op)){
                        $op = (float)$op;
                    }
                }
            }
            foreach($detail['rects'] as  &$rect){
                $fullX = ($rect['x'] == 0) || ($rect['x'] + $rect['width'] >= $detail['l']);
                $fullY = ($rect['y'] == 0) || ($rect['y'] + $rect['height'] >= $detail['h']);
                $fullXY = $fullX || $fullY;
                $fullZ = $rect['depth'] >= $thickness;
                $isButt = in_array($rect['side'],self::SIDE_BUTT);
                $rect['type'] = match (true) {
                    !$fullXY && !$fullZ, $isButt => self::RECT_TYPES['GROOVE'],
                    !$fullXY && $fullZ => self::RECT_TYPES['RECTANGLE'],
                    $fullXY && $fullZ => self::RECT_TYPES['P_SHAPE'],
                    default => self::RECT_TYPES['RABBET'],
                };
//                if($rect['type'] == self::RECT_TYPES['P_SHAPE']){
//                    $rect['offset'] = $fullX ? $rect['y'] : $rect['x'];
//                }
                $rect['depth'] = min($rect['depth'], $thickness);
                if(!$fullXY && $fullZ) {
                    $rect['depth'] = $rect['depth'] + Program::BORE_THROUGH_DZ_PLUS;
                    $rect['r'] = max($rect['r'], 10);
                }
                foreach ($rect as &$value){
                    if(is_numeric($value)){
                        $value = (float)$value;
                    }
                    if($value === ""){
                        $value = null;
                    }
                }
            }
        }
        return $project;
    }

}
