<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateS3userTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s3user', function (Blueprint $table) {
            $table->string('id', 50)->unique();
            $table->boolean('status')->unsigned()->default(0);
            $table->boolean('is_admin')->unsigned()->default(0);
            $table->boolean('notify')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s3user');
    }
}
