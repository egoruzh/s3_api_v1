<?php

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $max = env("MAX+FILE_SIZE_UPLOAD_MB",  8) * 1024;

        $mimes = env("MIME_TYPES_ENABLED", "image/jpeg,image/png,image/jpg,application/pdf,text/plain");
        switch ($this->method()) {
            case 'POST': {
                return [
                    'userFile'=>'required|array|min:1',
                    'userFile.*'=> "mimetypes:$mimes|max:$max",
                ];
            }
            default:{
                return [];
            }
        }
    }
    public function messages(): array
    {
        return [
            "userFile.required"=>"userFile[] fields is empty",
            "userFile.array"=>"The 'userFile' field must be an array.",
            "userFile.*.max" => "The :attribute must not be greater than :max kilobytes.",
            "userFile.*.mimetypes" => "The :attribute must be a file of type: :values",
        ];
    }
}
