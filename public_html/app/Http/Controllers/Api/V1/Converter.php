<?php

namespace App\Http\Controllers\Api\V1;

use Converter\ApiClientKronas;
use Converter\ConvertProjectToJson;
use Converter\Giblab;
use Converter\ProjectKronasDecoder;
use Exception;
use json2xml\Project;

class Converter
{
    public function index()
    {
        foreach (getallheaders() as $name => $value) {
            if ($name == 'Authorization') {
                $token = explode(" ", $value, 2);
                if (isset($token[1])) {
                    $_SESSION['token'] = $token[1];
                }
            }
        }
        $error = false;

// Takes raw data from the request
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == 'GET') {
            $requestArray = $_GET;
        } else {
            $input = file_get_contents('php://input');
            $requestArray = json_decode($input, 1) ?? false;
        }
        if (!$requestArray) {
            $result = [
                'error' => 'bad request',
                'status' => false,
            ];
            $error = true;
            $status = false;
            $this->sayResponse($result, 'bad_request');
        }
        $request['data'] = reset($requestArray);
        $request['action'] = key($requestArray);

        $actions = [
            'kronas_to_json' => [
                'method' => 'POST',
                'dataType' => 'string'
            ],
            'kronas_or_project_to_json' => [
                'method' => 'POST',
                'dataType' => 'string'
            ],
            'project_to_json' => [
                'method' => 'POST',
                'dataType' => 'string'
            ],
            'kronas_to_project' => [
                'method' => 'POST',
                'dataType' => 'string'
            ],
            'json_to_kronas_and_project' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'json_to_kronas' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'json_to_project' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'calc_edge' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'logout' => [
                'method' => 'GET',
                'dataType' => ''
            ],
            'login' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'save_to_my_collection' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'my_projects' => [
                'method' => 'GET',
                'dataType' => ''
            ],
            'my_project' => [
                'method' => 'GET',
                'dataType' => ''
            ],
            'get_pdf_cutting' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'xnc_pdf' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'lc4' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'mpr' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'ptx' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'gibcut' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'group' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'count_sheet_from_json' => [
                'method' => 'POST',
                'dataType' => 'array'
            ],
            'test' => [
                'method' => 'POST',
                'dataType' => ''
            ],
        ];


//$result = false;


        switch (true) {
            case !isset($request['action']):
                $error = 'the request must contain a field "action"';
                $status = false;
                break;
            case !in_array($request['action'], array_keys($actions), true):
                $error = 'field action valid one of the values: ' . implode(", ", array_keys($actions));
                $status = false;
                break;
            case $actions[$request['action']]['method'] !== $method:
                $error = 'This request must be with HTTP METHOD: ' . $actions[$request['action']]['method'];
                break;
            case $actions[$request['action']]['dataType'] === 'string' && !is_string($request['data']):
                $error = 'data is not string';
                break;
            case $actions[$request['action']]['dataType'] === 'array' && !is_array($request['data']):
                $error = 'data is not json';
                break;
            case $request['action'] === 'kronas_to_json':
                try {
                    $decode = ProjectKronasDecoder::decode($request['data']);
                    $result = (new ConvertProjectToJson($decode, true))->getResult();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'kronas_or_project_to_json':
                try {
                    $decode = ProjectKronasDecoder::decode($request['data']);
                } catch (Exception $e) {
                    $decode = $request['data'];
                }
                try {
                    $result = (new ConvertProjectToJson($decode, true))->getResult();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'calc_edge':

                $ch = curl_init('https://bd.kronas.com.ua/getPrice.php');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request['data']));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = json_decode(curl_exec($ch), true);
                curl_close($ch);
                break;
            case $request['action'] === 'project_to_json':
                try {
                    $result = (new ConvertProjectToJson($request['data'], true))->getResult();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'kronas_to_project':
                try {
                    $result = ProjectKronasDecoder::decode($request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'json_to_kronas_and_project':
                try {
                    $project = (new Project($request['data']))->getProject();
                    $result = [
                        'project' => $project,
                        'kronas' => ProjectKronasDecoder::encode($project),
                    ];
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'save_to_my_collection':
                $json = $request['data']['json'];
                $name = $request['data']['name'];
                try {
                    $result = (new ApiClientKronas())->addProject(json_encode($json), $name);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                $request['action'] = 'upload_kronas_to_my';
                break;
            case $request['action'] === 'json_to_kronas':
                try {
                    $project = (new Project($request['data']))->getProject();
                    $result = ProjectKronasDecoder::encode($project);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'json_to_project':
                try {
                    $result = (new Project($request['data']))->getProject();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'logout':
                try {
                    (new ApiClientKronas())->logout();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                unset($_SESSION['token']);
                header('Location: /save', 1, '301');
                break;
            case $request['action'] === 'login':
                try {
                    $result = (new ApiClientKronas())->login($request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'my_projects':
                try {
                    $result = (new ApiClientKronas())->myProjects();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'my_project':
                try {
                    $result = (new ApiClientKronas())->myProjectFile($request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                break;
            case $request['action'] === 'get_pdf_cutting':

                try {
                    $result = Giblab::getResultFromArray('cutting_pdf', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'xnc_pdf':
                try {
                    $result = Giblab::getResultFromArray('xnc_pdf', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'count_sheet_from_json':
                try {
                    $result = Giblab::getCountMaterialSheet($request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'lc4':
                try {
                    $result = Giblab::getResultFromArray('lc4', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'gibcut':
                try {
                    $result = Giblab::getResultFromArray('gibcut', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'ptx':
                try {
                    $result = Giblab::getResultFromArray('ptx', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'mpr':
                try {
                    $result = Giblab::getResultFromArray('mpr', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'group':
                try {
                    $result = Giblab::getResultFromArray('group', $request['data']);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    break;
                }
                break;
            case $request['action'] === 'test':
                $result = match ($request['data']) {
                    'php' => phpversion(),
                    default => 'ok',
                };

                break;
        }
        $result = $result ?? false;
        switch (true) {
            case isset($status):
                break;
            case $error !== false && $error !== '':
                $status = false;
                break;
            case isset($result['status']):
                $status = is_bool(
                    $result['status']
                ) ? $result['status'] : $result['status'] == 'true' || $result['status'] == 'ok';
                break;
            default:
                $status = true;
        }
        if ($status === false) {
            if (isset($result['message']) && $error === false) {
                $error = $result['message'];
            }
        }

        if ($request['data'] !== false || $request['action'] !== null) {
            header("Content-Type: application/json; charset=UTF-8");
            $error = json_decode($error) ?? $error;
            $this->sayResponse(['status' => $status, 'data' => $result, 'error' => $error], $request['action']);
        }
    }
    public function sayResponse(array $result, $action)
    {
        $response = json_encode($result, JSON_UNESCAPED_UNICODE);
        $level = match (true) {
            $result['status'] === true => 200,
            default => 200,
        };
//        (new Log)->add($level, $action, $result);
        if (!$result['status']) {
            http_response_code(400);
        }
        die($response);
    }
}
