<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Storage;
use Enigma\ValidatorTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin Builder
 * @property string $filename
 * @property string $s3root;
 * @property mixed $user_id
 * @property int|mixed $size
 * @property string $doctype
 * @property string $content_type
 * @property string $status
 * @property int $id
 * @property mixed $created_at
 * @property bool|mixed $is_remote
 */
class Document extends Model
{
    use ValidatorTrait;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $guarded = ['id'];

    public array $validationRules = ['size' => 'required|numeric'];

    public array $validationMessages = ['size.numeric' => 'not numeric'];

    public array $validationAttributes = ['status' => 'Status'];

    public function attributes()
    {
        return [
            'filename' => 'File Name',
            'status' => 'Status',
        ];
    }

    public function getUrlAttribute(): string
    {
        return Storage::disk('s3')->url($this->s3root);
    }

    public function getUploadedTimeAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(S3user::class, 'id');
    }

    protected static function boot(): void
    {
        parent::boot();
        static::creating(function ($query) {
            $query->doctype = $query->doctype ?? 'unknown';
            $query->status = $query->status ?? "in_queue";
        });
        static::softDeleted(function ($model) {
//            $old_name = $model->s3root;
            $model->s3root .= '.' . '.trash';
            $model->status = "trashed";
            $model->save();
//            Storage::disk(env('STORAGE_TYPE'))->move($old_name,$model->s3root);//not working ...
        });
        static::validateOnSaving();
    }

//
//    /**
//     * Code to be executed before the validation goes here.
//     */
//    public function beforeValidation()
//    {
//        // Some code goes here..
//    }
//
//    /**
//     * Code to be executed after the validation goes here.
//     */
//    public function afterValidation()
//    {
//        // Some code goes here..
//    }
}
