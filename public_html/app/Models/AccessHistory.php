<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $user_id
 * @property mixed|string $root
 * @property mixed|string|null $type_access
 * @property mixed|string|null $route
 * @property false|mixed|string $request
 */
class AccessHistory extends Model
{
    use HasFactory;

    protected $table = 'access_history';

}
