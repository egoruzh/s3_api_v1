<?php

namespace App\Models;

use Enigma\ValidatorTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;

/**
 * @mixin Builder
 * @method static firstOrCreate(array $array, int[] $array1)
 * @method static firstWhere(string $string, $id)
 * @property mixed $id
 */
class S3user extends Model
{

    use ValidatorTrait;

    public $incrementing = false;
    public array $validationRules = [
        'id' => 'uuid'
    ];
    public array $validationMessages = [
        'id.uuid' => 'user id not valid. must uuid type (b1a8ae8c-c997-11ed-afa1-0242ac120001)'
    ];
    public array $validationAttributes = [
        'id' => 'Id'
    ];
    protected $keyType = 'string';
    protected $table = 's3user';
    protected $fillable = ['id', 'status'];
    private mixed $separator = '/';

    protected static function boot(): void
    {
        parent::boot();

        // Add this method for validating the current model on model saving event
        static::validateOnSaving();
    }

    public function getUserFiles(string $path, bool $recursive = true): array
    {
        $result = [];
        $request_count_dir = count(explode('/', $path));
        $files = $this->files()->where('s3root', 'like', "$path/%")->get();
        foreach ($files as $file_root) {
            if (!$recursive && false === (substr_count($file_root['s3root'], '/') === $request_count_dir)) {
                continue;
            }
            $result[$file_root->s3root] = [
                'type' => 'file',
                'name' => pathinfo($file_root->s3root, PATHINFO_FILENAME),
                'path' => pathinfo($this->stripPathPrefix($file_root->s3root, $path), PATHINFO_DIRNAME),
                'size' => $file_root->size,
                'uploadedTime' => $file_root->uploadedTime,
                'doctype' => $file_root->doctype,
                'created_at' => $file_root->created_at->toDateTimeString()
            ];
        }
        return array_values($result);
    }

    public function files(): HasMany
    {
        return $this->hasMany(Document::class, 'user_id');
    }

    public function stripPathPrefix(string $path, string $requestDir): string
    {
        $path = $this->separator . ltrim($path, $this->separator);
        return substr($path, strlen($this->separator . trim($requestDir, $this->separator) . $this->separator));
    }

    public function getUserFolders(string $path, bool $recursive, string $requestDir): array
    {
        $result = [];
        $request_count_dir = count(explode('/', $path));
        $files = array_column($this->files()->where('s3root', 'like', "$path/%")->get()->toArray(), 's3root');
        foreach ($files as $file) {
            $c = count(explode('/', $file));
            if ($c - 1 === $request_count_dir) {
                continue;
            }
            if (!$recursive) {
                $name = $this->stripPathPrefix($path . '/' . explode('/', $file)[$request_count_dir], $requestDir);
            } else {
                $name = $this->stripPathPrefix(pathinfo($file, PATHINFO_DIRNAME), $requestDir);
            }
            $result[$name] = [
                'type' => 'dir',
                'name' => $name,
            ];
        }

        return array_values($result);
    }

    public function scopeActive($query)
    {
        return $query->whereStatus('1');
    }

    public function scopeAdmin($query)
    {
        return $query->whereIsAdmin('1');
    }

    public function scopeNotify($query)
    {
        return $query->whereNotify('1');
    }

    public function attachRoles($roles): void
    {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function attachRole($role): void
    {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->attach($role);
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function detachRoles($roles): void
    {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function detachRole($role): void
    {
        if (is_object($role)) {
            $role = $role->getKey();
        }
        if (is_array($role)) {
            $role = $role['id'];
        }
        $this->roles()->detach($role);
    }

    public function hasAccess($permissions): bool
    {
        return $this->hasPermission($permissions);
    }

    public function hasPermission($permissions): bool
    {
        if (!is_array($permissions)) {
            $permissions = (array)$permissions;
        }
        $allPermissions = $this->pluck('name')->toArray();
        foreach ($permissions as $permission) {
            if (!in_array($permission['name'], $allPermissions, true)) {
                return false;
            }
        }
        return true;
    }

    public function checkFolderExist(string $requestDir): bool
    {
        $files = $this->files()->where('s3root', 'like', "$requestDir/%")->get();
        return $files->count() > 0;
    }

    public function getRoles()
    {
        $roles = [];
        if ($this->roles()) {
            $roles = $this->roles()->get();
        }
        return $roles;
    }

}
