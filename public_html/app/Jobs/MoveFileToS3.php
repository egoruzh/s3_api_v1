<?php

namespace App\Jobs;

use App\Models\Document;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\File;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class MoveFileToS3 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $filename;
    protected $documentId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filename, $documentId)
    {
        $this->filename = $filename . '.enc';
        $this->documentId = $documentId;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $result = '';
        $storagType = env('STORAGE_TYPE');
        // Upload file to S3
        if($storagType === 's3') {
            $result = Storage::disk('s3')->putFileAs(
                '/',
                new File(storage_path('app' . '/' . $this->filename)),
                $this->filename
            );
        }
        // Forces collection of any existing garbage cycles
        // If we don't add this, in some cases the file remains locked
        gc_collect_cycles();
        if ($storagType === 's3' && !$result) {
            throw new Exception("Couldn't upload file to S3");
        }else{
            $doc = Document::query()->find($this->documentId);
            $doc->status = "uploaded";
            $doc->save();
        }

        // delete file from local filesystem
        if ($storagType === 's3' && !Storage::disk('local')->delete($this->filename)) {
            throw new Exception('File could not be deleted from the local filesystem ');
        }
    }
}
