<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kronas convert test</title>
</head>
<body style="background-color: #eceeef">
<div class="container-fluid">
    <div class="pt-2 ml-10">
        <div class="form-inline" id="form_actions">
            <a class="pl-3 pr-3" href="template.kronas" download>.kronas</a>
            <input accept=".kronas,.project,.xml" type="file" id="kronas-file" class="form-control" name="userfile"/>
            <input type="button" class="btn btn-primary m-2 js-event" data-action="kronas_or_project_to_json" value="kronas or project to json"
                   disabled="disabled"/>
            <input type="button" class="btn btn-primary m-2 js-event" data-action="kronas_to_project" value="kronas to project"
                   disabled="disabled"/>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <div id="json-renderer" style="font-size: 0.8em!important;line-height: 1.2;!important;"></div>
            <div id="json-renderer2" style="font-size: 0.8em!important;line-height: 1.2;!important;"></div>
        </div>
        <div class="col">
            <div id="xml-renderer" style="word-break: break-word!important;"></div>
        </div>
    </div>
</div>
<script src="{{ asset("js/app.js") }}"></script>
</body>
<div id="overlay_loader" style="z-index: 999;">
    <div id="loader"></div>
</div>
</html>
