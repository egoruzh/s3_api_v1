import 'bootstrap';
import 'bootstrap-css';
import '../css/loader.css';
import 'jquery-confirm';
import 'jquery-jsonview';
import "../css/pace.css";
import "../css/simpleXML.css";
import "jquery-confirm/css/jquery-confirm.css";
import 'jquery-jsonview/dist/jquery.jsonview.css';
import './simpleXML';

const domElementRenderJson2 = document.getElementById('json-renderer2');
const domElementRenderJson = document.getElementById('json-renderer');
const domElementRenderXml = document.getElementById('xml-renderer');
const domElementForm = document.getElementById('form_actions');

const btnToggleSum = document.createElement("button");
btnToggleSum.setAttribute('class', 'btn btn-primary m-2');
btnToggleSum.innerHTML = "Toggle";

const btnToggle = document.createElement("button");
btnToggle.innerHTML = "Toggle";
const btnRun = document.getElementsByClassName("js-event");
const btnCalculateEdge = document.createElement("button");

const domRenderXml = document.createElement("div");
btnCalculateEdge.setAttribute('class', 'btn btn-primary m-2');
btnCalculateEdge.setAttribute('id', 'calc_edge');
btnCalculateEdge.setAttribute('data-action', 'calc_edge');
btnCalculateEdge.innerHTML = "Calculate sum";

const count_sheet_from_json = document.createElement("button");
count_sheet_from_json.setAttribute('class', 'btn btn-primary m-2');
count_sheet_from_json.setAttribute('id', 'count_sheet_from_json');
count_sheet_from_json.setAttribute('data-action', 'count_sheet_from_json');
count_sheet_from_json.innerHTML = "count sheet from json";

const get_pdf_cutting = document.createElement("button");
get_pdf_cutting.setAttribute('class', 'btn btn-primary m-2');
get_pdf_cutting.setAttribute('id', 'get_pdf_cutting');
get_pdf_cutting.setAttribute('data-action', 'get_pdf_cutting');
get_pdf_cutting.innerHTML = "get pdf cutting";

const btnSendJsonToConvert = document.createElement("button");
btnSendJsonToConvert.setAttribute('class', 'btn btn-primary m-2');
btnSendJsonToConvert.setAttribute('id', 'convert_json_to_kronas');
btnSendJsonToConvert.setAttribute('data-action', 'convert_json_to_kronas');
btnSendJsonToConvert.innerHTML = "json to project";

const btnDownloadProject = document.createElement("a");
btnDownloadProject.setAttribute('class', 'btn btn-primary m-2');
btnDownloadProject.innerHTML = "download .project";

const btnDownloadKronas = document.createElement("a");
btnDownloadKronas.setAttribute('class', 'btn btn-primary m-2');
btnDownloadKronas.innerHTML = "download .kronas";

const btnDownloadJson = document.createElement("a");
btnDownloadJson.setAttribute('class', 'btn btn-primary m-2');
btnDownloadJson.innerHTML = "download json";

const btn_project_program = document.createElement("button");
btn_project_program.setAttribute('class', 'btn btn-primary m-2');
btn_project_program.setAttribute('id', 'project_program');
btn_project_program.innerHTML = "Project programs";


const kronasfile = document.getElementById("kronas-file");
const fileReader = new FileReader();
const resultContent = {};

$( document ).ajaxStart(function() {
    $('#overlay_loader').fadeIn();
});

$( document ).ajaxComplete(function() {
    $('#overlay_loader').fadeOut();
});

function clearResults() {
    domElementRenderJson.innerText = '';
    domElementRenderJson2.innerText = '';
    domElementRenderXml.innerText = '';
    if(this.files.length !== 0){
        document.querySelector('[data-action^="kronas_or_project_to_json"]').removeAttribute('disabled');
        if (this.files[0].name.split('.').pop() === 'kronas'){
            document.querySelector('[data-action^="kronas_to_project"]').removeAttribute('disabled');
        } else {
            document.querySelector('[data-action^="kronas_to_project"]').setAttribute('disabled', 'disabled');
        }
    } else {
        document.querySelector('[data-action^="kronas_to_project"]').setAttribute('disabled', 'disabled');
        document.querySelector('[data-action^="get_pdf_cutting"]').setAttribute('disabled', 'disabled');
        document.querySelector('[data-action^="count_sheet_from_json"]').setAttribute('disabled', 'disabled');
        document.querySelector('[data-action^="kronas_or_project_to_json"]').setAttribute('disabled', 'disabled');
    }


    if (document.getElementById("calc_edge")) {
        document.getElementById("calc_edge").remove()
    }
    if (document.getElementById("count_sheet_from_json")) {
        document.getElementById("count_sheet_from_json").remove()
    }
    if (document.getElementById("get_pdf_cutting")) {
        document.getElementById("get_pdf_cutting").remove()
    }
    if (document.getElementById("project_program")) {
        document.getElementById("project_program").remove()
    }
}

function getProjectProgram() {
    const operations = createElementFromHTML(resultContent.projectData).getElementsByTagName("operation");
    domElementRenderXml.innerText = '';
    for (var i = 0; i < operations.length; i++) {
        if (operations[i].getAttribute("program") !== null) {
            $(domElementRenderXml).simpleXML({xmlString: operations[i].getAttribute("program")});
        }
    }
}
function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();

    // Change this to div.childNodes to support multiple top-level nodes.
    return div.firstChild;
}
function convert_json_to_kronas() {
    domRenderXml.innerText = '';
    $.ajax({
        url: '/src/index.php',
        method: 'post',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({ json_to_kronas_and_project: resultContent.jsonData}),
        success: function (e) {
            if (e.status === true) {
                $(domRenderXml).simpleXML({xmlString: e.data.project})
                resultContent.project = e.data.project;
                resultContent.kronas = e.data.kronas;

                let xmlUriComponent = "data:application/xml;charset=utf-8," + encodeURIComponent(e.data.project);
                btnDownloadProject.setAttribute("href", xmlUriComponent);
                btnDownloadProject.setAttribute('download', 'convert.project');
                btnDownloadKronas.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(e.data.kronas));
                btnDownloadKronas.setAttribute('download', 'convert.kronas');

                domRenderXml.prepend(btnDownloadProject);
                domRenderXml.prepend(btnDownloadKronas);
            }

            $.confirm({
                type: 'purple',
                title: 'Result',
                columnClass: 'large',
                content: e.status === true ? domRenderXml : e.error,
                backgroundDismiss: true,
                buttons: {
                    close: {
                        btnClass: 'btn-purple',
                        action: function () {
                        }
                    }
                }
            });
        }
    });
}

const getResult = function () {
    event.preventDefault();
    if (event.target.getAttribute('data-action') === 'kronas_or_project_to_json') {
        fileReader.onload = function () {
            $.ajax({
                url: '/src/index.php',
                method: 'post',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({ kronas_or_project_to_json: fileReader.result}),
                success: function (e) {
                    if (e.status !== true) {
                        $.alert({
                            title: 'Error!',
                            content: e.error,
                            backgroundDismiss: true,
                        });
                        return;
                    }
                    domElementRenderJson.innerText = '';
                    if (document.getElementById("calc_edge")) {
                        document.getElementById("calc_edge").remove();
                    }
                    if (document.getElementById("count_sheet_from_json")) {
                        document.getElementById("count_sheet_from_json").remove();
                    }
                    if (document.getElementById("get_pdf_cutting")) {
                        document.getElementById("get_pdf_cutting").remove();
                    }
                    btnDownloadJson.setAttribute("href", "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(e.data)));
                    btnDownloadJson.setAttribute('download', 'convert.json');
                    resultContent.jsonData = e.data;

                    $(domElementRenderJson).JSONView(e.data, {collapse: 2});
                    domElementForm.append(btnCalculateEdge);
                    get_pdf_cutting.removeAttribute('disabled')
                    domElementForm.append(get_pdf_cutting);
                    count_sheet_from_json.removeAttribute('disabled')
                    domElementForm.append(count_sheet_from_json);
                    domElementRenderJson.prepend(btnDownloadJson);
                    btnSendJsonToConvert.addEventListener("click", convert_json_to_kronas);
                    domElementRenderJson.prepend(btnSendJsonToConvert);
                    domElementRenderJson.prepend(btnToggle);
                },
                error: function (e){
                    console.log(e)
                    $.alert({
                        title: 'Error!',
                        content: e.responseJSON.error,
                        backgroundDismiss: true,
                    });
                }
            });

        };

        if(kronasfile.files.length){
            fileReader.readAsText(kronasfile.files[0]);
        }else {
            $.alert({
                title: 'Error!',
                content: 'file not loaded',
                backgroundDismiss: true,
            });
        }

    }
    if (event.target.getAttribute('data-action') === 'kronas_to_project') {
        fileReader.onload = function () {
            $.ajax({
                url: '/src/index.php',
                method: 'post',
                contentType: "application/json",
                dataType: 'json',
                data: JSON.stringify({ kronas_to_project: fileReader.result}),
                success: function (e) {
                    if (e.status === true) {
                        domElementRenderXml.innerText = '';
                        let xmlUriComponent = "data:application/xml;charset=utf-8," + encodeURIComponent(e.data);
                        btnDownloadProject.setAttribute("href", xmlUriComponent);
                        btnDownloadProject.setAttribute('download', 'convert.project');
                        resultContent.projectData = e.data;
                        $(domElementRenderXml).simpleXML({xmlString: e.data});
                        domElementRenderXml.prepend(btnDownloadProject);
                        btn_project_program.addEventListener("click", getProjectProgram);
                        domElementForm.append(btn_project_program);
                    } else {
                        $.alert({
                            title: 'Error!',
                            content: e.error,
                            backgroundDismiss: true,
                        });
                    }
                },
                error: function (e){
                    console.log(e)
                    $.alert({
                        title: 'Error!',
                        content: e.responseJSON.error,
                        backgroundDismiss: true,
                    });
                }
            });

        };
        if(kronasfile.files.length){
            fileReader.readAsText(kronasfile.files[0]);
        }else {
            $.alert({
                title: 'Error!',
                content: 'file not loaded',
                backgroundDismiss: true,
            });
        }
    }
    if (event.target.getAttribute('data-action') === 'calc_edge') {
        $.ajax({
            url: '/src/index.php',
            method: 'post',
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify({ calc_edge: resultContent.jsonData}),
            success: function (e) {
                $.confirm({
                    type: 'purple',
                    title: 'Sum: ' + e.data[e.data.length - 1].ordersum,
                    columnClass: 'large',
                    content: $(domElementRenderJson2).JSONView(e.data, {
                        collapsed: true,
                        nl2br: true,
                        recursive_collapser: true
                    }),
                    backgroundDismiss: true,
                    buttons: {
                        close: {
                            btnClass: 'btn-purple',
                            action: function () {
                            }
                        }
                    },
                    onContentReady: function () {
                        this.setContentPrepend(btnToggleSum);
                    },
                });
            },
            error: function (e){
                console.log(e)
                $.alert({
                    title: 'Error!',
                    content: e.responseJSON.error,
                    backgroundDismiss: true,
                });
            }
        });
    }
    if (event.target.getAttribute('data-action') === 'get_pdf_cutting') {
        domElementRenderJson2.innerHTML = '';
        $.ajax({
            url: '/src/index.php',
            method: 'post',
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify({ get_pdf_cutting: resultContent.jsonData}),
            success: function (e) {
                $.confirm({
                    type: 'purple',
                    title: 'files pdf cutting',
                    columnClass: 'large',
                    content: $(domElementRenderJson2).JSONView(e.data, {
                        collapsed: true,
                        nl2br: true,
                        recursive_collapser: true
                    }),
                    backgroundDismiss: true,
                    buttons: {
                        close: {
                            btnClass: 'btn-purple',
                            action: function () {
                            }
                        }
                    },
                    onContentReady: function () {

                    },
                });
            },
            error: function (e){
                console.log(e)
                $.alert({
                    title: 'Error!',
                    content: e.responseJSON.error,
                    backgroundDismiss: true,
                });
            }
        });
    }
    if (event.target.getAttribute('data-action') === 'count_sheet_from_json') {
        domElementRenderJson2.innerHTML = '';
        $.ajax({
            url: '/src/index.php',
            method: 'post',
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify({ count_sheet_from_json: resultContent.jsonData}),
            success: function (e) {
                $.confirm({
                    type: 'purple',
                    title: 'files pdf cutting',
                    columnClass: 'large',
                    content: $(domElementRenderJson2).JSONView(e.data, {
                        collapsed: true,
                        nl2br: true,
                        recursive_collapser: true
                    }),
                    backgroundDismiss: true,
                    buttons: {
                        close: {
                            btnClass: 'btn-purple',
                            action: function () {
                            }
                        }
                    },
                    onContentReady: function () {
                        this.setContentPrepend(btnToggleSum);
                    },
                });
            },
            error: function (e){
                $.alert({
                    title: 'Error!',
                    content: e.responseJSON.error,
                    backgroundDismiss: true,
                });
            }
        });
    }
};

kronasfile.addEventListener('change', clearResults);
btnCalculateEdge.addEventListener('click', getResult, false);
get_pdf_cutting.addEventListener('click', getResult, false);
count_sheet_from_json.addEventListener('click', getResult, false);
function toggleJsonSum() {
    $('#json-renderer2').JSONView('toggle');
}

function toggleJson() {
    $('#json-renderer').JSONView('toggle');
}

btnToggle.addEventListener('click', toggleJson, false);
btnToggleSum.addEventListener('click', toggleJsonSum, false);

for (let i = 0; i < btnRun.length; i++) {
    btnRun[i].addEventListener('click', getResult, false);
}